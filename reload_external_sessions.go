package confreader

import (
	"fmt"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.uber.org/zap"
	"gopkg.in/robfig/cron.v2"
)

func reloadExternalUserSessions(sources []storage.ResourceNameT,
	cl APIExternalUserSessionV1sGetter,
	st LocalExternalUserSessionV1sSetter) error {
	var srvData = make([]storage.ExternalUserSessionV1, 0)

	for _, src := range sources {
		s, err := cl.GetExternalUserSessions(src, nil)
		if err != nil {
			return fmt.Errorf("can not load API ExternalUserSessionListV1: %w", err)
		}

		srvData = append(srvData, s...)
	}

	if err := st.SetExternalUserSessionV1s(srvData); err != nil {
		return fmt.Errorf("can not update internal ExternalUserSessionListV1 storage: %w", err)
	}

	return nil
}

func wrapReloadExternalUserSessions(
	sources []storage.ResourceNameT,
	cl APIExternalUserSessionV1sGetter,
	st LocalExternalUserSessionV1sSetter,
	logger *zap.Logger) cron.Job {
	iLog := logger.With(zap.String("job", "ReloadExternalUserSessions"))

	return cron.FuncJob(func() {
		if err := reloadExternalUserSessions(sources, cl, st); err != nil {
			iLog.Error("can not reload ExternalUserSessionsV1", zap.Error(err))
		} else {
			iLog.Info("Reloaded ExternalUserSessionsV1")
		}
	})
}

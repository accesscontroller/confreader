package confreader

import (
	"fmt"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.uber.org/zap"
	"gopkg.in/robfig/cron.v2"
)

func reloadWebResourceV1s(
	resourcesGet APIWebResourceV1sGetter,
	categoriesGet APIWebResourceCategoryV1sGetter,
	st LocalWebResourceV1sSetter) error {
	// Load WebResourceCategoryV1s.
	categories, err := categoriesGet.GetWebResourceCategories(nil)
	if err != nil {
		return fmt.Errorf("can not load API WebResourceCategoryV1s: %w", err)
	}

	// Load WebResourceV1s for every category and update.
	var resources = make([]storage.WebResourceV1, 0)

	for i := range categories {
		pCat := &categories[i]

		// Load.
		refRes, err := resourcesGet.GetWebResourceCategoryWebResources(pCat.Metadata.Name, nil)
		if err != nil {
			return fmt.Errorf(
				"can not load API WebResourceV1s for WebResourceCategoryV1 %s: %w",
				pCat.Metadata.Name, err)
		}

		for j := range refRes {
			resources = append(resources, refRes[j])
		}
	}

	// Update WebResources.
	if err := st.SetWebResourcesV1Tx(resources); err != nil {
		return fmt.Errorf("can not update internal WebResourceV1s storage: %w", err)
	}

	return nil
}

func wrapReloadWebResourceV1s(
	resourcesGet APIWebResourceV1sGetter,
	categoriesGet APIWebResourceCategoryV1sGetter,
	st LocalWebResourceV1sSetter,
	logger *zap.Logger) cron.Job {
	iLog := logger.With(zap.String("job", "ReloadWebResourceV1s"))

	return cron.FuncJob(func() {
		if err := reloadWebResourceV1s(
			resourcesGet, categoriesGet, st); err != nil {
			iLog.Error("Can not reload WebResourceV1s", zap.Error(err))
		} else {
			iLog.Info("Reloaded WebResourceV1s")
		}
	})
}

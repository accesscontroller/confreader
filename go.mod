module gitlab.com/accesscontroller/confreader

go 1.15

require (
	gitlab.com/accesscontroller/accesscontroller/controller/storage v0.0.0-20201030165051-959181aa057a
	gitlab.com/accesscontroller/confreader/types v0.0.0-20200919124612-98bd190c2adf
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
	gopkg.in/robfig/cron.v2 v2.0.0-20150107220207-be2e0b0deed5
)

package confreader

import (
	"net"
	"net/url"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/confreader/types"
)

// APIAccessGroupV1sGetter defines an interface which loads AccessGroupV1s from API.
type APIAccessGroupV1sGetter interface {
	// GetAccessGroups loads AccessGroupV1s.
	GetAccessGroups(names []storage.ResourceNameT) ([]storage.AccessGroupV1, error)
}

// LocalAccessGroupV1sSetter defines an interface which saves AccessGroupV1s to local storage.
type LocalAccessGroupV1sSetter interface {
	// SetAccessGroupV1s saves AccessGroupV1s.
	SetAccessGroupV1s([]storage.AccessGroupV1) error
}

// APIExternalGroupV1sGetter defines an interface which loads ExternalGroupV1s from API.
type APIExternalGroupV1sGetter interface {
	// GetExternalGroups loads ExternalGroupV1s.
	GetExternalGroups(names []storage.ResourceNameT, source storage.ResourceNameT) ([]storage.ExternalGroupV1, error)
}

// LocalExternalGroupV1sSetter defines an interface which saves ExternalGroupV1s to local storage.
type LocalExternalGroupV1sSetter interface {
	// SetExternalGroupV1s saves ExternalGroupV1s.
	SetExternalGroupV1s([]storage.ExternalGroupV1) error
}

// APIExternalGroup2ExternalUserListRelationsV1Getter defines an interface
// which loads GetExternalGroup2ExternalUserListRelationsV1 from API.
type APIExternalGroup2ExternalUserListRelationsV1Getter interface {
	// GetExternalGroup2ExternalUserListRelationsV1 loads ExternalGroup2ExternalUserListRelationsV1.
	GetExternalGroup2ExternalUserListRelationsV1(
		name, source storage.ResourceNameT) (*storage.ExternalGroup2ExternalUserListRelationsV1, error)
}

// LocalExternalGroup2ExternalUserListRelationsV1Setter defines an interface
// which saves ExternalGroup2ExternalUserListRelationsV1 to local storage.
type LocalExternalGroup2ExternalUserListRelationsV1Setter interface {
	// SetExternalGroup2ExternalUserListRelationsV1 saves ExternalGroup2ExternalUserListRelationsV1.
	SetExternalGroup2ExternalUserListRelationsV1([]*storage.ExternalGroup2ExternalUserListRelationsV1) error
}

// APIExternalUserSessionV1sGetter defines an interface which loads ExternalUserSessionV1s from API.
type APIExternalUserSessionV1sGetter interface {
	// GetExternalUserSessions loads ExternalUserSessionV1s.
	GetExternalUserSessions(source storage.ResourceNameT,
		filter []storage.ExternalUserSessionFilterV1) ([]storage.ExternalUserSessionV1, error)
}

// LocalExternalUserSessionV1sSetter defines an interface which loads ExternalUserSessionV1s from API.
type LocalExternalUserSessionV1sSetter interface {
	// SetExternalUserSessionV1s saves ExternalUserSessionV1s.
	SetExternalUserSessionV1s(sessions []storage.ExternalUserSessionV1) error
}

// APIWebResourceCategoryV1sGetter defines an interface which loads ExternalGroupV1s from API.
type APIWebResourceCategoryV1sGetter interface {
	// GetWebResourceCategories loads WebResourceCategoryV1.
	GetWebResourceCategories(filter []storage.WebResourceCategoryFilterV1) ([]storage.WebResourceCategoryV1, error)
}

// APIWebResourceV1sGetter defines an interface which loads WebResourceV1s for one WebResourceCategoryV1 from API.
type APIWebResourceV1sGetter interface {
	// GetWebResourceCategoryWebResources loads WebResourceV1s.
	GetWebResourceCategoryWebResources(
		category storage.ResourceNameT, filters []storage.WebResourceFilterV1) ([]storage.WebResourceV1, error)
}

// LocalWebResourceV1sSetter defines an interface which saves WebResourceV1s to local storage.
type LocalWebResourceV1sSetter interface {
	// SetWebResourcesV1Tx saves WebResourceV1s.
	SetWebResourcesV1Tx(resources []storage.WebResourceV1) error
}

// APIAccessGroup2WebResourceCategoryListRelationsV1Getter defines an interface
// which loads AccessGroup2WebResourceCategoryListRelationsV1 from API for one AccessGroupV1.
type APIAccessGroup2WebResourceCategoryListRelationsV1Getter interface {
	// GetAccessGroup2WebResourceCategoryListRelationsV1 loads AccessGroup2WebResourceCategoryListRelationsV1
	// for one AccessGroupV1.
	GetAccessGroup2WebResourceCategoryListRelationsV1(
		accessGroup storage.ResourceNameT) (*storage.AccessGroup2WebResourceCategoryListRelationsV1, error)
}

// LLocalAccessGroup2WebResourceCategoryListRelationsV1Setter defines an interface
// which saves AccessGroup2WebResourceCategoryListRelationsV1 to local storage.
type LocalAccessGroup2WebResourceCategoryListRelationsV1Setter interface {
	// SetAccessGroup2WebResourceCategoryListRelationsV1 saves
	// AccessGroup2WebResourceCategoryListRelationsV1.
	SetAccessGroup2WebResourceCategoryListRelationsV1(
		relations []*storage.AccessGroup2WebResourceCategoryListRelationsV1) error
}

// APIExternalGroupV1sExternalUsersV1sGetter defines an interfaces which loads ExternalGroupV1s
// and their ExternalUserV1s members from API.
type APIExternalGroupV1sExternalUsersV1sGetter interface {
	APIExternalGroupV1sGetter
	APIExternalGroup2ExternalUserListRelationsV1Getter
}

// APIGetter defines an interface which loads all the required resources from API.
type APIGetter interface {
	APIAccessGroup2WebResourceCategoryListRelationsV1Getter
	APIAccessGroupV1sGetter
	APIExternalGroup2ExternalUserListRelationsV1Getter
	APIExternalGroupV1sGetter
	APIExternalUserSessionV1sGetter
	APIWebResourceCategoryV1sGetter
	APIWebResourceV1sGetter
}

// LocalExternalGroupV1sExternalUsersV1sSetter defines an interface
// which saves ExternalGroupV1s and their ExternalUserV1s members to Local storage.
type LocalExternalGroupV1sExternalUsersV1sSetter interface {
	LocalExternalGroupV1sSetter
	LocalExternalGroup2ExternalUserListRelationsV1Setter
}

// LocalSetter defines an interface which saves all the required resources to local storage.
type LocalSetter interface {
	LocalAccessGroup2WebResourceCategoryListRelationsV1Setter
	LocalAccessGroupV1sSetter
	LocalExternalGroupV1sSetter
	LocalExternalUserSessionV1sSetter
	LocalExternalGroup2ExternalUserListRelationsV1Setter
	LocalWebResourceV1sSetter
}

// LocalUserURLChecker defines an interface which checks a User access permissions to a URL.
type LocalUserURLChecker interface {
	// CheckAccessUserURL checks a User access to a URL.
	CheckAccessUserURL(source, username storage.ResourceNameT, u *url.URL) (*types.CheckResult, error)
}

// LocalUserDomainChecker defines an interface which checks a User access permissions to a Domain.
type LocalUserDomainChecker interface {
	// CheckAccessUserDomain checks a User access to a Domain.
	CheckAccessUserDomain(source, username storage.ResourceNameT, domain storage.WebResourceDomainT) (*types.CheckResult, error)
}

// LocalUserIPChecker defines an interface which checks a User access permissions to an IP.
type LocalUserIPChecker interface {
	// CheckAccessUserIP checks a User access to an IP.
	CheckAccessUserIP(source, username storage.ResourceNameT, addr net.IP) (*types.CheckResult, error)
}

// LocalSIPURLChecker defines an interface which checks a source IP access permissions to a URL.
type LocalSIPURLChecker interface {
	// CheckAccessSIPURL checks a source IP access to a URL.
	CheckAccessSIPURL(srcIP net.IP, u *url.URL) (*types.CheckResult, error)
}

// LocalSIPDomainChecker defines an interface which checks a source IP access permissions to a Domain.
type LocalSIPDomainChecker interface {
	// CheckAccessSIPDomain checks a source IP access to a Domain.
	CheckAccessSIPDomain(srcIP net.IP, domain storage.WebResourceDomainT) (*types.CheckResult, error)
}

// LocalSIPIPChecker defines an interface which checks a source IP access permissions to an IP.
type LocalSIPIPChecker interface {
	// CheckAccessSIPDIP checks a source IP access to an IP.
	CheckAccessSIPDIP(srcIP, dstIP net.IP) (*types.CheckResult, error)
}

// LocalChecker defines an interface which allows Access Checks.
type LocalChecker interface {
	LocalUserDomainChecker
	LocalUserIPChecker
	LocalUserURLChecker
	LocalSIPURLChecker
	LocalSIPDomainChecker
	LocalSIPIPChecker
}

// LocalStorage defines an interface which allows Access Checks and a local storage manipulations.
type LocalStorage interface {
	LocalSetter
	LocalChecker
}

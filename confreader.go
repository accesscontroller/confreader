package confreader

import (
	"context"
	"fmt"
	"net"
	"net/url"
	"time"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/confreader/types"
	"go.uber.org/zap"
	"gopkg.in/robfig/cron.v2"
)

// Default Intervals are simple numbers intentionally.
const (
	DefaultFullReloadInterval        = time.Minute * 31
	DefaultUsersGroupsReloadInterval = time.Minute * 5
	DefaultWebResourceReloadInterval = time.Minute * 11
	DefaultAccessRulesReloadInterval = time.Minute * 17
	DefaultSessionsReloadInterval    = time.Minute * 1
)

type TimersConfig struct {
	FullReload         time.Duration
	UsersGroupsReload  time.Duration
	WebResourcesReload time.Duration
	AccessGroupsReload time.Duration
	SessionsReload     time.Duration
}

type ConfReader struct {
	cl APIGetter
	st LocalStorage

	timings *TimersConfig
	logger  *zap.Logger

	usersGroupsSources []storage.ResourceNameT
	sessionsSources    []storage.ResourceNameT
}

func NewDefault(usersGroupsSources, sessionsSources []storage.ResourceNameT,
	cl APIGetter, st LocalStorage) (*ConfReader, error) {
	// Logger.
	logger, err := zap.NewProduction()
	if err != nil {
		return nil, err
	}

	return New(
		usersGroupsSources, sessionsSources,
		cl, st, &TimersConfig{
			FullReload:         DefaultFullReloadInterval,
			UsersGroupsReload:  DefaultUsersGroupsReloadInterval,
			WebResourcesReload: DefaultWebResourceReloadInterval,
			AccessGroupsReload: DefaultUsersGroupsReloadInterval,
			SessionsReload:     DefaultSessionsReloadInterval,
		},
		logger), nil
}

func New(usersGroupsSources, sessionsSources []storage.ResourceNameT,
	cl APIGetter, st LocalStorage, timings *TimersConfig, logger *zap.Logger) *ConfReader {
	// Configure logger.
	logger = logger.With(zap.String("package", "confreader"))

	return &ConfReader{
		cl:                 cl,
		st:                 st,
		timings:            timings,
		sessionsSources:    sessionsSources,
		usersGroupsSources: usersGroupsSources,
		logger:             logger,
	}
}

func (cr *ConfReader) InitReloadLoops(ctx context.Context) {
	iLog := cr.logger.With(zap.String("module", "ReloadLoops"))

	runner := cron.New()

	// AccessGroupV1s properties.
	accessGroupsReloadSch := cron.Every(cr.timings.AccessGroupsReload)
	accessGroupsReloadJob := wrapReloadAccessGroupsV1(cr.cl, cr.st, iLog)
	jobID := runner.Schedule(accessGroupsReloadSch, accessGroupsReloadJob)
	iLog.Info("Created AccessGroupV1 reload job.", zap.Int("ID", int(jobID)), zap.Any("schedule", accessGroupsReloadSch))

	// ExternalGroupV1s and ExternalUserV1s.
	externalGroupReloadSch := cron.Every(cr.timings.UsersGroupsReload)
	externalGroupReloadJob := wrapReloadExternalGroupsV1(cr.usersGroupsSources, cr.cl, cr.st, iLog)
	jobID = runner.Schedule(externalGroupReloadSch, externalGroupReloadJob)
	iLog.Info("Created ExternalGroupV1 reload job",
		zap.Int("ID", int(jobID)), zap.Any("schedule", externalGroupReloadSch))

	// ExternalUserSessionV1s.
	externalUserSessionsReloadSch := cron.Every(cr.timings.SessionsReload)
	externalUserSessionsReloadJob := wrapReloadExternalUserSessions(cr.sessionsSources, cr.cl, cr.st, iLog)
	jobID = runner.Schedule(externalUserSessionsReloadSch, externalUserSessionsReloadJob)
	iLog.Info("Created ExternalUserSessionsV1 reload job",
		zap.Int("ID", int(jobID)), zap.Any("schedule", externalUserSessionsReloadSch))

	// WebResourceV1s and WebResourceCategoryV1s.
	webResourceCatReloadSch := cron.Every(cr.timings.WebResourcesReload)
	webResourceCatReloadJob := cron.FuncJob(func() {
		wrapReloadAccessGroup2WebResourceCategoryListRelationsV1(cr.cl, cr.cl, cr.st, iLog)
		wrapReloadWebResourceV1s(cr.cl, cr.cl, cr.st, cr.logger)
	})
	jobID = runner.Schedule(webResourceCatReloadSch, webResourceCatReloadJob)
	iLog.Info("Created WebResourceCategoryV1 reload job",
		zap.Int("ID", int(jobID)), zap.Any("schedule", webResourceCatReloadSch))

	// Start all Reload tasks for the first time.
	accessGroupsReloadJob.Run()
	externalGroupReloadJob.Run()
	externalUserSessionsReloadJob.Run()
	webResourceCatReloadJob.Run()

	runner.Start()

	iLog.Info("Started Reload Loops Scheduler")

	go func() {
		<-ctx.Done()

		iLog.Info("Stopping Reload Loops Scheduler")

		runner.Stop()
		iLog.Info("Stopped Reload Loops Scheduler")
	}()
}

// CheckAccessUserURL checks a User access to a URL.
func (cr *ConfReader) CheckAccessUserURL(source, username storage.ResourceNameT, u *url.URL) (types.CheckAccessResultGetter, error) {
	st, err := cr.st.CheckAccessUserURL(source, username, u)
	if err != nil {
		return nil, fmt.Errorf("can not Check User access to URL: %w", err)
	}

	return st, nil
}

// CheckAccessUserDomain checks a User access to a Domain.
func (cr *ConfReader) CheckAccessUserDomain(source, username storage.ResourceNameT,
	domain string) (types.CheckAccessResultGetter, error) {
	st, err := cr.st.CheckAccessUserDomain(source, username, storage.WebResourceDomainT(domain))
	if err != nil {
		return nil, fmt.Errorf("can not Check User access to Domain: %w", err)
	}

	return st, nil
}

// CheckAccessUserIP checks a User access to an IP.
func (cr *ConfReader) CheckAccessUserIP(source, username storage.ResourceNameT, addr net.IP) (types.CheckAccessResultGetter, error) {
	st, err := cr.st.CheckAccessUserIP(source, username, addr)
	if err != nil {
		return nil, fmt.Errorf("can not Check User access to IP: %w", err)
	}

	return st, nil
}

// CheckAccessSIPURL checks a source IP access to a URL.
func (cr *ConfReader) CheckAccessSIPURL(srcIP net.IP, u *url.URL) (types.CheckAccessResultGetter, error) {
	st, err := cr.st.CheckAccessSIPURL(srcIP, u)
	if err != nil {
		return nil, fmt.Errorf("can not Check source IP access to dst URL: %w", err)
	}

	return st, nil
}

// CheckAccessSIPDomain checks a source IP access to a Domain.
func (cr *ConfReader) CheckAccessSIPDomain(srcIP net.IP, domain string) (types.CheckAccessResultGetter, error) {
	st, err := cr.st.CheckAccessSIPDomain(srcIP, storage.WebResourceDomainT(domain))
	if err != nil {
		return nil, fmt.Errorf("can not Check source IP access to dst domain: %w", err)
	}

	return st, nil
}

// CheckAccessSIPDIP checks a source IP access to an IP.
func (cr *ConfReader) CheckAccessSIPDIP(srcIP, dstIP net.IP) (types.CheckAccessResultGetter, error) {
	st, err := cr.st.CheckAccessSIPDIP(srcIP, dstIP)
	if err != nil {
		return nil, fmt.Errorf("can not Check source IP access to dst IP: %w", err)
	}

	return st, nil
}

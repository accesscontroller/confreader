package types

import (
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// CheckAccessResultGetter defines a type which provides information about a check.
type CheckAccessResultGetter interface {
	// IsAllowed returns Access Permissions status.
	IsAllowed() bool

	// GetWebResourceInfo returns a WebResourceV1 info.
	GetWebResourceInfo() (resource, category storage.ResourceNameT)

	// GetUserInfo returns an ExternalUserV1 info.
	GetUserInfo() (user, source storage.ResourceNameT)

	// GetAccessGroup returns an AccessGroupV1 which enforced the CheckResult.
	GetAccessGroup() *storage.AccessGroupV1
}

// CheckResult contains result from one access check.
type CheckResult struct {
	// Allowed - is access allowed.
	Allowed bool

	// WebResource name.
	WebResource storage.ResourceNameT

	// WebResourceCategory name
	WebResourceCategory storage.ResourceNameT

	// AccessGroup which was applied for the Check.
	AccessGroup *storage.AccessGroupV1

	// User - detected user name.
	User storage.ResourceNameT

	// Source - detected user source.
	Source storage.ResourceNameT
}

// IsAllowed returns Access Permissions status.
func (c *CheckResult) IsAllowed() bool {
	return c.Allowed
}

// GetWebResourceInfo returns a WebResourceV1 info.
func (c *CheckResult) GetWebResourceInfo() (resource, category storage.ResourceNameT) {
	return c.WebResource, c.WebResourceCategory
}

// GetUserInfo returns an ExternalUserV1 info.
func (c *CheckResult) GetUserInfo() (user, source storage.ResourceNameT) {
	return c.User, c.Source
}

// GetAccessGroup returns an AccessGroupV1 which enforced the CheckResult.
func (c *CheckResult) GetAccessGroup() *storage.AccessGroupV1 {
	return c.AccessGroup
}

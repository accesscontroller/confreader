package confreader

import (
	"fmt"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.uber.org/zap"
	"gopkg.in/robfig/cron.v2"
)

func reloadAccessGroup2WebResourceCategoryListRelationsV1(
	relsGet APIAccessGroup2WebResourceCategoryListRelationsV1Getter,
	accessGroupsGet APIAccessGroupV1sGetter,
	st LocalAccessGroup2WebResourceCategoryListRelationsV1Setter) error {
	// Load AccessGroupV1s.
	groups, err := accessGroupsGet.GetAccessGroups(nil)
	if err != nil {
		return fmt.Errorf("can not load API AccessGroupV1s: %w", err)
	}

	// Load relations.
	var relations = make([]*storage.AccessGroup2WebResourceCategoryListRelationsV1, 0, len(groups))

	for i := range groups {
		rels, err := relsGet.GetAccessGroup2WebResourceCategoryListRelationsV1(groups[i].Metadata.Name)
		if err != nil {
			return fmt.Errorf(
				"can not load API AccessGroup2WebResourceCategoryListRelationsV1 from AccessGroup %s: %w",
				groups[i].Metadata.Name, err)
		}

		relations = append(relations, rels)
	}

	// Update Local Storage.
	err = st.SetAccessGroup2WebResourceCategoryListRelationsV1(relations)
	if err != nil {
		return fmt.Errorf("can not update internal AccessGroup2WebResourceCategoryListRelationsV1 storage: %w", err)
	}

	return nil
}

func wrapReloadAccessGroup2WebResourceCategoryListRelationsV1(
	relsGet APIAccessGroup2WebResourceCategoryListRelationsV1Getter,
	accessGroupsGet APIAccessGroupV1sGetter,
	st LocalAccessGroup2WebResourceCategoryListRelationsV1Setter,
	logger *zap.Logger) cron.Job {
	return cron.FuncJob(func() {
		iLog := logger.With(zap.String("job", "ReloadAccessGroup2WebResourceCategoryListRelationsV1"))

		if err := reloadAccessGroup2WebResourceCategoryListRelationsV1(
			relsGet, accessGroupsGet, st); err != nil {
			iLog.Error("can not reload AccessGroup2WebResourceCategoryListRelationsV1", zap.Error(err))
		} else {
			iLog.Info("Reloaded AccessGroup2WebResourceCategoryListRelationsV1")
		}
	})
}

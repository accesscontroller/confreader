package db

import (
	"fmt"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.etcd.io/bbolt"
)

func getGroupAccessGroupBktTx(tx *bbolt.Tx) (*bbolt.Bucket, error) {
	var bkt = tx.Bucket(_GroupAccessGroupBKT)
	if bkt == nil {
		return nil, fmt.Errorf("%w: Group => AccessGroup Bucket", ErrNoBucket)
	}

	return bkt, nil
}

func convertGroupAccessGroup(groups []storage.ExternalGroupV1) map[storage.ResourceNameT]storage.ResourceNameT {
	var result = make(map[storage.ResourceNameT]storage.ResourceNameT, len(groups))

	for i := range groups {
		pg := &groups[i]

		result[pg.Metadata.ExternalSource.SourceName+";"+pg.Metadata.Name] = pg.Data.AccessGroupName
	}

	return result
}

func loadAllGroupAccessGroupsBkt(bkt *bbolt.Bucket) (map[storage.ResourceNameT]storage.ResourceNameT, error) {
	var result = make(map[storage.ResourceNameT]storage.ResourceNameT, bkt.Stats().KeyN)

	if err := bkt.ForEach(func(k, v []byte) error {
		result[storage.ResourceNameT(k)] = storage.ResourceNameT(v)

		return nil
	}); err != nil {
		return nil, err
	}

	return result, nil
}

func setExternalGroupV1sTx(tx *bbolt.Tx, groups []storage.ExternalGroupV1) error {
	// Bucket.
	bkt, err := getGroupAccessGroupBktTx(tx)
	if err != nil {
		return err
	}

	// Convert.
	var reference = convertGroupAccessGroup(groups)

	// Load.
	dbGroups, err := loadAllGroupAccessGroupsBkt(bkt)
	if err != nil {
		return err
	}

	// Delete.
	for dbK := range dbGroups {
		_, ok := reference[dbK]
		if ok {
			continue
		}

		if err := bkt.Delete([]byte(dbK)); err != nil {
			return err
		}
	}

	// Create/Update.
	for refK, refV := range reference {
		dbV, ok := dbGroups[refK]
		if ok { // Possible update.
			// Check for update.
			if dbV == refV {
				continue
			}
		}

		// Create/Update.
		if err := bkt.Put([]byte(refK), []byte(refV)); err != nil {
			return err
		}
	}

	return nil
}

// getGroupsAccessGroupsTx loads all AccessGroupV1s names for given ExternalGroupV1s.
// Names are returned as []byte slices to reduce string <=> []byte conversions.
func getGroupsAccessGroupsTx(tx *bbolt.Tx, groups ...[]byte) ([][]byte, error) {
	// Get Bucket.
	bkt, err := getGroupAccessGroupBktTx(tx)
	if err != nil {
		return nil, err
	}

	// Load.
	var accessGroups [][]byte

	for _, gr := range groups {
		rawAG := bkt.Get(gr)
		if rawAG == nil {
			// No AccessGroupV1s for given ExternalGroupV1s is not error:
			// The ExternalGroupV1 just does not have AccessGroupV1 name
			// configured.
			continue
		}

		accessGroups = append(accessGroups, rawAG)
	}

	return accessGroups, nil
}

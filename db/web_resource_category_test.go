package db

import (
	"net"
	"net/url"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.etcd.io/bbolt"
)

type TestSetWebResourcesV1TxSuite struct {
	db *DB

	suite.Suite
}

func (ts *TestSetWebResourcesV1TxSuite) SetupTest() {
	// Faker config.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNowf("Can not init Faker", "Error: %s", err)
	}

	db, err := initDBTest()
	if err != nil {
		ts.FailNow("Can not open DB", err)
	}

	ts.db = db

	// Pre-create some data - IP => WebResourceV1.
	if err := ts.db.db.Batch(func(tx *bbolt.Tx) error {
		bkt, err := getIPsBktTx(tx)
		if err != nil {
			return err
		}

		// Put Data.
		var data = map[_IP]storage.ResourceNameT{
			encodeIP(net.ParseIP("10.8.1.1")): "web resource 1",
			encodeIP(net.ParseIP("10.8.1.1")): "web resource 1",
			encodeIP(net.ParseIP("10.8.2.1")): "web resource 2",
		}

		for k, v := range data {
			if err := bkt.Put(k[:], []byte(v)); err != nil {
				return err
			}
		}

		return nil
	}); err != nil {
		ts.FailNowf("Can not pre-create required data", "Error: %s", err)
	}

	// Pre-create some data - Domain => WebResourceV1.
	if err := ts.db.db.Batch(func(tx *bbolt.Tx) error {
		bkt, err := getDomainsBktTx(tx)
		if err != nil {
			return err
		}

		// Put Data.
		var data = map[storage.WebResourceDomainT]storage.ResourceNameT{
			"example-1-1.com": "web resource 1",
			"example-1-2.com": "web resource 1",
			"example-2-1.com": "web resource 2",
		}

		for k, v := range data {
			if err := bkt.Put([]byte(k), []byte(v)); err != nil {
				return err
			}
		}

		return nil
	}); err != nil {
		ts.FailNowf("Can not pre-create required data", "Error: %s", err)
	}

	// Pre-create some data - URL => WebResourceV1.
	if err := ts.db.db.Batch(func(tx *bbolt.Tx) error {
		bkt, err := getURLsBktTx(tx)
		if err != nil {
			return err
		}

		// Put Data.
		var data = map[string]storage.ResourceNameT{
			"http://example-1-1.com/url-1": "web resource 1",
			"http://example-1-2.com/url-2": "web resource 1",
			"http://example-2-1.com/url-2": "web resource 2",
		}

		for k, v := range data {
			if err := bkt.Put([]byte(k), []byte(v)); err != nil {
				return err
			}
		}

		return nil
	}); err != nil {
		ts.FailNowf("Can not pre-create required data", "Error: %s", err)
	}

	// Pre-create some data - WebResourceV1 => WebResourceCategoryV1.
	if err := ts.db.db.Batch(func(tx *bbolt.Tx) error {
		bkt, err := getWebResourceToWebResourceCategoriesBktTx(tx)
		if err != nil {
			return err
		}

		// Put Data.
		var data = map[string]storage.ResourceNameT{
			"web resource 1": "web resource category 1",
			"web resource 2": "web resource category 1",
			"web resource 3": "web resource category 2",
		}

		for k, v := range data {
			if err := bkt.Put([]byte(k), []byte(v)); err != nil {
				return err
			}
		}

		return nil
	}); err != nil {
		ts.FailNowf("Can not pre-create required data", "Error: %s", err)
	}
}

func (ts *TestSetWebResourcesV1TxSuite) TestSetSuccess() {
	var test = struct {
		Resources []storage.WebResourceV1
	}{}

	// Fake.
	if err := faker.FakeData(&test); err != nil {
		ts.FailNowf("Can not FakeData", "Error: %s", err)
	}

	// Add some data for update check.
	test.Resources = append(test.Resources, storage.WebResourceV1{
		Metadata: storage.MetadataV1{
			Name: "web resource 2",
		},
		Data: storage.WebResourceDataV1{
			Domains: []storage.WebResourceDomainT{
				"example-2-1.com",
			},
			IPs: []net.IP{
				net.ParseIP("10.8.2.1"),
			},
			URLs: []url.URL{
				parseURLPanic("http://example-2-1.com/url-2"),
			},
		},
	})

	// Test.
	err := ts.db.SetWebResourcesV1Tx(test.Resources)

	if !ts.NoError(err, "Must not return error") {
		ts.FailNow("Got error - can not continue")
	}

	// Load every resource type and check it.
	err = ts.db.db.Batch(func(tx *bbolt.Tx) error {
		// IPs.
		// Expected.
		var refIPs = make(map[string]storage.ResourceNameT)

		for i := range test.Resources {
			pr := &test.Resources[i]

			for _, IP := range pr.Data.IPs {
				refIPs[IP.String()] = pr.Metadata.Name
			}
		}

		// Database.
		ipBkt, err := getIPsBktTx(tx)
		if err != nil {
			ts.FailNowf("Can not get IP Bucket", "Error: %s", err)
		}

		// Load from DB with IP converted to string.
		var dbIPs = make(map[string]storage.ResourceNameT, ipBkt.Stats().KeyN)

		if err := ipBkt.ForEach(func(k, v []byte) error {
			dbIPs[net.IP(k).String()] = storage.ResourceNameT(v)

			return nil
		}); err != nil {
			ts.FailNowf("Can not load all DB IP => WebResourceV1s", "Error: %s", err)
		}

		ts.Equal(refIPs, dbIPs, "Unexpected IP set got from DB")

		// Domains.
		// Expected.
		var refDomains = make(map[storage.WebResourceDomainT]storage.ResourceNameT)

		for i := range test.Resources {
			pr := &test.Resources[i]

			for j := range pr.Data.Domains {
				refDomains[pr.Data.Domains[j]] = pr.Metadata.Name
			}
		}

		// Database.
		domainBkt, err := getDomainsBktTx(tx)
		if err != nil {
			ts.FailNowf("Can not get Domain Bucket", "Error: %s", err)
		}

		// Load from DB with IP converted to string.
		var dbDomains = make(map[storage.WebResourceDomainT]storage.ResourceNameT, domainBkt.Stats().KeyN)

		if err := domainBkt.ForEach(func(k, v []byte) error {
			dbDomains[storage.WebResourceDomainT(k)] = storage.ResourceNameT(v)

			return nil
		}); err != nil {
			ts.FailNowf("Can not load all DB Domain => WebResourceV1s", "Error: %s", err)
		}

		ts.Equal(refDomains, dbDomains, "Unexpected Domain set got from DB")

		// URLs.
		// Expected.
		var refURLs = make(map[string]storage.ResourceNameT)

		for i := range test.Resources {
			pr := &test.Resources[i]

			for j := range pr.Data.URLs {
				refURLs[pr.Data.URLs[j].String()] = pr.Metadata.Name
			}
		}

		// Database.
		URLBkt, err := getURLsBktTx(tx)
		if err != nil {
			ts.FailNowf("Can not get URL Bucket", "Error: %s", err)
		}

		// Load from DB with IP converted to string.
		var dbURLs = make(map[string]storage.ResourceNameT, URLBkt.Stats().KeyN)

		if err := URLBkt.ForEach(func(k, v []byte) error {
			dbURLs[string(k)] = storage.ResourceNameT(v)

			return nil
		}); err != nil {
			ts.FailNowf("Can not load all DB URL => WebResourceV1s", "Error: %s", err)
		}

		ts.Equal(refURLs, dbURLs, "Unexpected URL set got from DB")

		// WebResource => WebResourceCategory.
		// Expected.
		var refWebResCats = make(map[storage.ResourceNameT]storage.ResourceNameT)

		for i := range test.Resources {
			pr := &test.Resources[i]

			refWebResCats[pr.Metadata.Name] = pr.Data.WebResourceCategoryName
		}

		// Database.
		webResCats, err := getWebResourceToWebResourceCategoriesBktTx(tx)
		if err != nil {
			ts.FailNowf("Can not get WebResource => WebResourceCategory Bucket", "Error: %s", err)
		}

		// Load from DB with IP converted to string.
		var dbWResCats = make(map[storage.ResourceNameT]storage.ResourceNameT, webResCats.Stats().KeyN)

		if err := webResCats.ForEach(func(k, v []byte) error {
			dbWResCats[storage.ResourceNameT(k)] = storage.ResourceNameT(v)

			return nil
		}); err != nil {
			ts.FailNowf("Can not load all DB WebResourceV1 => WebResourceCategoryV1", "Error: %s", err)
		}

		ts.Equal(refWebResCats, dbWResCats, "Unexpected WebResource => WebResourceCategory set got from DB")

		return nil
	})

	ts.NoError(err, "Must not return error")
}

func (ts *TestSetWebResourcesV1TxSuite) TearDownTest() {
	if err := ts.db.db.Close(); err != nil {
		ts.FailNow("Can not close DB", err)
	}
}

func TestSetWebResourcesV1Tx(t *testing.T) {
	suite.Run(t, &TestSetWebResourcesV1TxSuite{})
}

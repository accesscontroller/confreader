package db

import (
	"net"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.etcd.io/bbolt"
)

type TestSetExternalUserSessionV1sTxSuite struct {
	db *DB

	suite.Suite
}

func (ts *TestSetExternalUserSessionV1sTxSuite) SetupTest() {
	// Faker config.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNowf("Can not init Faker", "Error: %s", err)
	}

	db, err := initDBTest()
	if err != nil {
		ts.FailNow("Can not open DB", err)
	}

	ts.db = db

	// Pre-create some data.
	if err := ts.db.db.Batch(func(tx *bbolt.Tx) error {
		bkt, err := getSessionsBktTx(tx)
		if err != nil {
			return err
		}

		// Put Data.
		var data = map[_IP]storage.ResourceNameT{
			encodeIP(net.ParseIP("10.8.0.1")): "user name 1",
			encodeIP(net.ParseIP("10.8.0.2")): "user name 1",
			encodeIP(net.ParseIP("10.8.1.2")): "user name 2",
			encodeIP(net.ParseIP("10.8.1.3")): "user name 3",
			encodeIP(net.ParseIP("10.8.1.4")): "source 1" + ";" + "user name 1",
		}

		for k, v := range data {
			if err := bkt.Put(k[:], []byte(v)); err != nil {
				return err
			}
		}

		return nil
	}); err != nil {
		ts.FailNowf("Can not pre-create required data", "Error: %s", err)
	}
}

func (ts *TestSetExternalUserSessionV1sTxSuite) TearDownTest() {
	if err := ts.db.db.Close(); err != nil {
		ts.FailNow("Can not close DB", err)
	}
}

func (ts *TestSetExternalUserSessionV1sTxSuite) TestSetSuccess() {
	var test = struct {
		Sessions []storage.ExternalUserSessionV1
	}{}

	// Fake.
	if err := faker.FakeData(&test); err != nil {
		ts.FailNowf("Can not FakeData", "Error: %s", err)
	}

	// Add data to trigger update.
	test.Sessions = append(test.Sessions, storage.ExternalUserSessionV1{
		Metadata: storage.MetadataV1{
			APIVersion: storage.APIVersionV1Value,
			Name:       "session 101",
		},
		Data: storage.ExternalUserSessionDataV1{
			ExternalUserName:              "user name 1",
			ExternalUsersGroupsSourceName: "source 1",
			IPAddress:                     net.ParseIP("10.8.1.4"),
		},
	})

	// Test.
	err := ts.db.SetExternalUserSessionV1s(test.Sessions)

	if !ts.NoError(err, "Must not return Set error") {
		ts.FailNow("Got error - can not continue")
	}

	expectedGroups, err := convertIPsToSessions(test.Sessions)
	if err != nil {
		ts.FailNowf("Can not convertIPToSessions", "Error: %q", err)
	}

	var (
		dbGroups map[_IP][]byte
	)

	// Check DB data.
	err = ts.db.db.Batch(func(tx *bbolt.Tx) error {
		bkt, err := getSessionsBktTx(tx)
		if err != nil {
			return err
		}

		grs, err := loadAllSessions(bkt)
		if err != nil {
			return err
		}

		dbGroups = grs

		return nil
	})

	ts.Equal(expectedGroups, dbGroups, "Unexpected ExternalUserSessionV1s loaded from DB")
}

func TestSetExternalUserSessionV1sTx(t *testing.T) {
	suite.Run(t, &TestSetExternalUserSessionV1sTxSuite{})
}

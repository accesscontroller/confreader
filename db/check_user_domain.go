package db

import (
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/confreader/types"
	"go.etcd.io/bbolt"
)

func checkAccessUserDomainTx(tx *bbolt.Tx, source, username storage.ResourceNameT, domain storage.WebResourceDomainT) (
	*types.CheckResult, error) {
	// Find a first AccessGroupV1 for given User.
	userAccessGroup, err := getUserFirstAccessGroupV1Tx(tx, source, username)
	if err != nil {
		return nil, err
	}

	// Lookup for Domain's Info.
	domainInfo, err := getDomainAccessGroupV1sNamesTx(tx, domain)
	if err != nil {
		return nil, err
	}

	return &types.CheckResult{
		Allowed:             computeAccessPolicyResult(userAccessGroup, domainInfo.accessGroups),
		AccessGroup:         userAccessGroup,
		Source:              source,
		User:                username,
		WebResource:         domainInfo.resource,
		WebResourceCategory: domainInfo.category,
	}, nil
}

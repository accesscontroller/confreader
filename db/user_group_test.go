package db

import (
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.etcd.io/bbolt"
)

type TestSetExternalGroup2ExternalUserListRelationsV1TxSuite struct {
	db *DB

	suite.Suite
}

func (ts *TestSetExternalGroup2ExternalUserListRelationsV1TxSuite) SetupTest() {
	// Faker config.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNowf("Can not init Faker", "Error: %s", err)
	}

	db, err := initDBTest()
	if err != nil {
		ts.FailNow("Can not open DB", err)
	}

	ts.db = db

	// Pre-create some data.
	if err := ts.db.db.Batch(func(tx *bbolt.Tx) error {
		bkt, err := getUserGroupsBktTx(tx)
		if err != nil {
			return err
		}

		// Put Data.
		var data = map[storage.ResourceNameT][][]byte{
			"source 1;user 1": {
				[]byte("source 1;group 1"),
				[]byte("source 1;group 2"),
			},
			"source 1;user 2": {
				[]byte("source 1;group 1"),
			},
			"source 1;user 3": {
				[]byte("source 1;group 2"),
			},
			"source 2;user 1": {
				[]byte("source 2;group 3"),
			},
		}

		for k, v := range data {
			bts, err := encodeUserGroups(v)
			if err != nil {
				return err
			}

			if err := bkt.Put([]byte(k), bts); err != nil {
				return err
			}
		}

		return nil
	}); err != nil {
		ts.FailNowf("Can not pre-create required data", "Error: %s", err)
	}
}

func (ts *TestSetExternalGroup2ExternalUserListRelationsV1TxSuite) TearDownTest() {
	if err := ts.db.db.Close(); err != nil {
		ts.FailNow("Can not close DB", err)
	}
}

func (ts *TestSetExternalGroup2ExternalUserListRelationsV1TxSuite) TestSetSuccess() {
	var test = struct {
		Relations []*storage.ExternalGroup2ExternalUserListRelationsV1
	}{}

	// Fake.
	if err := faker.FakeData(&test); err != nil {
		ts.FailNowf("Can not FakeData", "Error: %s", err)
	}

	// Add data to trigger update.
	test.Relations = append(test.Relations,
		&storage.ExternalGroup2ExternalUserListRelationsV1{
			Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
				ExternalGroup: "group 3",
				ExternalUsers: []storage.ResourceNameT{
					"user 1",
				},
				ExternalUsersGroupsSource: "source 2",
			},
		},
		&storage.ExternalGroup2ExternalUserListRelationsV1{
			Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
				ExternalGroup: "group 2",
				ExternalUsers: []storage.ResourceNameT{
					"user 1",
				},
				ExternalUsersGroupsSource: "source 4",
			},
		},
	)

	// Test.
	err := ts.db.SetExternalGroup2ExternalUserListRelationsV1(test.Relations)

	if !ts.NoError(err, "Must not return Set error") {
		ts.FailNow("Got error - can not continue")
	}

	var (
		expectedRelations = convertGroupUsersRelations(test.Relations)
		dbUserGroups      map[storage.ResourceNameT][][]byte
	)

	// Check DB data.
	err = ts.db.db.Batch(func(tx *bbolt.Tx) error {
		bkt, err := getUserGroupsBktTx(tx)
		if err != nil {
			return err
		}

		grs, err := loadAllUserGroupsBkt(bkt)
		if err != nil {
			return err
		}

		dbUserGroups = grs

		return nil
	})

	ts.Equal(expectedRelations, dbUserGroups, "Unexpected Relations loaded from DB")
}

func TestSetExternalGroup2ExternalUserListRelationsV1Tx(t *testing.T) {
	suite.Run(t, &TestSetExternalGroup2ExternalUserListRelationsV1TxSuite{})
}

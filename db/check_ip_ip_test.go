package db

import (
	"net"
	"net/url"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/confreader/types"
	"go.etcd.io/bbolt"
)

type TestCheckIPIPSuite struct {
	db *DB

	suite.Suite
}

func (ts *TestCheckIPIPSuite) SetupTest() {
	// Faker config.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNowf("Can not init Faker", "Error: %s", err)
	}

	db, err := initDBTest()
	if err != nil {
		ts.FailNow("Can not open DB", err)
	}

	ts.db = db

	// Pre-create some data.
	if err := ts.db.db.Batch(func(tx *bbolt.Tx) error {
		// ExternalGroupV1s.
		if err := setExternalGroupV1sTx(tx, []storage.ExternalGroupV1{
			{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
					},
					Name: "group 1 at source 1",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 1",
				},
			},
			{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
					},
					Name: "group 2 at source 1",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 1",
				},
			},
			{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
					},
					Name: "group 3 at source 1",
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 2",
				},
			},
		}); err != nil {
			return err
		}

		// ExternalUserV1s and Groups Membership.
		if err := setExternalGroup2ExternalUserListRelationsV1Tx(tx, []*storage.ExternalGroup2ExternalUserListRelationsV1{
			{
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup: "group 1 at source 1",
					ExternalUsers: []storage.ResourceNameT{
						"user 1 at source 1",
						"user 2 at source 1",
					},
					ExternalUsersGroupsSource: "source 1",
				},
			},
			{
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup: "group 1 at source 2",
					ExternalUsers: []storage.ResourceNameT{
						"user 1 at source 2",
						"user 2 at source 2",
					},
					ExternalUsersGroupsSource: "source 2",
				},
			},
			{
				Data: storage.ExternalGroup2ExternalUserListRelationsDataV1{
					ExternalGroup: "group 3 at source 1",
					ExternalUsers: []storage.ResourceNameT{
						"user 3 at source 1",
					},
					ExternalUsersGroupsSource: "source 1",
				},
			},
		}); err != nil {
			return err
		}

		// AccessGroupV1s.
		if err := setAccessGroupV1sPropertiesTx(tx, []storage.AccessGroupV1{
			{
				Metadata: storage.MetadataV1{
					Name:       "access group 1",
					APIVersion: storage.APIVersionV1Value,
					ETag:       1,
					Kind:       storage.AccessGroupKind,
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyDeny,
					ExtraAccessDenyMessage:  "access group 1 access denied",
					OrderInAccessRulesList:  1,
					TotalGroupSpeedLimitBps: 100_000,
					UserGroupSpeedLimitBps:  1_000,
				},
			},
			{
				Metadata: storage.MetadataV1{
					Name:       "access group 2",
					APIVersion: storage.APIVersionV1Value,
					ETag:       2,
					Kind:       storage.AccessGroupKind,
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyAllow,
					ExtraAccessDenyMessage:  "access group 2 access denied",
					OrderInAccessRulesList:  2,
					TotalGroupSpeedLimitBps: 100_002,
					UserGroupSpeedLimitBps:  1_002,
				},
			},
		}); err != nil {
			return err
		}

		// WebResourceV1s.
		if err := setWebResourcesV1Tx(tx, []storage.WebResourceV1{
			{
				Metadata: storage.MetadataV1{
					Name: "web resource 1",
				},
				Data: storage.WebResourceDataV1{
					Description: "resource 1 at cat 1",
					Domains: []storage.WebResourceDomainT{
						"cat1-1.com",
						"cat1-2.com",
					},
					IPs: []net.IP{
						parseIPPanic("10.1.0.1"),
						parseIPPanic("10.1.0.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://cat1-1.com/index.html"),
						parseURLPanic("http://cat1-2.com/index.html"),
					},
					WebResourceCategoryName: "web resource category 1",
				},
			},
			{
				Metadata: storage.MetadataV1{
					Name: "web resource 2",
				},
				Data: storage.WebResourceDataV1{
					Description: "resource 2 at category 2",
					Domains: []storage.WebResourceDomainT{
						"cat2-1.com",
						"cat2-2.com",
					},
					IPs: []net.IP{
						parseIPPanic("10.2.0.1"),
						parseIPPanic("10.2.0.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://cat2-1.com/index.html"),
						parseURLPanic("http://cat2-2.com/index.html"),
					},
					WebResourceCategoryName: "web resource category 2",
				},
			},
			{
				Metadata: storage.MetadataV1{
					Name: "web resource 3",
				},
				Data: storage.WebResourceDataV1{
					Description: "resource 3 at cat 1",
					Domains: []storage.WebResourceDomainT{
						"cat3-1.com",
						"cat3-2.com",
					},
					IPs: []net.IP{
						parseIPPanic("10.3.0.1"),
						parseIPPanic("10.3.0.2"),
					},
					URLs: []url.URL{
						parseURLPanic("http://cat3-1.com/index.html"),
						parseURLPanic("http://cat3-2.com/index.html"),
					},
					WebResourceCategoryName: "web resource category 3",
				},
			},
		}); err != nil {
			return err
		}

		// WebResourceCategoryV1s and AccessGroupV1s.
		if err := setAccessGroup2WebResourceCategoryListRelationsV1Tx(tx, []*storage.AccessGroup2WebResourceCategoryListRelationsV1{
			{
				Data: storage.AccessGroup2WebResourceCategoryListRelationsDataV1{
					AccessGroup: "access group 1",
					Categories: []storage.ResourceNameT{
						"web resource category 1",
					},
				},
			},
			{
				Data: storage.AccessGroup2WebResourceCategoryListRelationsDataV1{
					AccessGroup: "access group 2",
					Categories: []storage.ResourceNameT{
						"web resource category 1",
						"web resource category 2",
					},
				},
			},
		}); err != nil {
			return err
		}

		// ExternalUserSessionV1s.
		if err := setExternalUserSessionV1sTx(tx, []storage.ExternalUserSessionV1{
			{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "sessions source 1",
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "session 1 at source 1",
				},
				Data: storage.ExternalUserSessionDataV1{
					ExternalUserName:              "user 1 at source 1",
					ExternalUsersGroupsSourceName: "source 1",
					Hostname:                      "host 1",
					IPAddress:                     parseIPPanic("10.200.0.1"),
				},
			},
			{
				Metadata: storage.MetadataV1{
					APIVersion:       storage.APIVersionV1Value,
					ETag:             1,
					ExternalResource: true,
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "sessions source 1",
						Kind:       storage.ExternalSessionsSourceKind,
					},
					Kind: storage.ExternalUserSessionKind,
					Name: "session 2 at source 1",
				},
				Data: storage.ExternalUserSessionDataV1{
					ExternalUserName:              "user 3 at source 1",
					ExternalUsersGroupsSourceName: "source 1",
					Hostname:                      "host 1",
					IPAddress:                     parseIPPanic("10.200.0.2"),
				},
			},
		}); err != nil {
			return err
		}

		return nil
	}); err != nil {
		ts.FailNowf("Can not pre-create required data", "Error: %s", err)
	}
}

func (ts *TestCheckIPIPSuite) TearDownTest() {
	if err := ts.db.db.Close(); err != nil {
		ts.FailNow("Can not close DB", err)
	}
}

func (ts *TestCheckIPIPSuite) TestCheckIPIPAccess() {
	var tests = []*struct {
		clientIP net.IP

		dstIP net.IP

		expectedInfo *types.CheckResult
	}{
		{
			clientIP: net.ParseIP("10.200.0.1"),
			dstIP:    parseIPPanic("10.1.0.1"),
			expectedInfo: &types.CheckResult{
				Allowed: true,
				AccessGroup: &storage.AccessGroupV1{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.AccessGroupKind,
						Name:       "access group 1",
						ETag:       1,
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyDeny,
						ExtraAccessDenyMessage:  "access group 1 access denied",
						OrderInAccessRulesList:  1,
						TotalGroupSpeedLimitBps: 100_000,
						UserGroupSpeedLimitBps:  1_000,
					},
				},
				Source:              "source 1",
				User:                "user 1 at source 1",
				WebResource:         "web resource 1",
				WebResourceCategory: "web resource category 1",
			},
		},
		{
			clientIP: net.ParseIP("10.200.0.1"),
			dstIP:    parseIPPanic("10.2.0.1"),
			expectedInfo: &types.CheckResult{
				Allowed: false,
				AccessGroup: &storage.AccessGroupV1{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.AccessGroupKind,
						Name:       "access group 1",
						ETag:       1,
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyDeny,
						ExtraAccessDenyMessage:  "access group 1 access denied",
						OrderInAccessRulesList:  1,
						TotalGroupSpeedLimitBps: 100_000,
						UserGroupSpeedLimitBps:  1_000,
					},
				},

				Source:              "source 1",
				User:                "user 1 at source 1",
				WebResource:         "web resource 2",
				WebResourceCategory: "web resource category 2",
			},
		},
		{
			clientIP: net.ParseIP("10.200.0.2"),
			dstIP:    parseIPPanic("10.3.0.1"),
			expectedInfo: &types.CheckResult{
				Allowed: true,
				AccessGroup: &storage.AccessGroupV1{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.AccessGroupKind,
						Name:       "access group 2",
						ETag:       2,
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyAllow,
						ExtraAccessDenyMessage:  "access group 2 access denied",
						OrderInAccessRulesList:  2,
						TotalGroupSpeedLimitBps: 100_002,
						UserGroupSpeedLimitBps:  1_002,
					},
				},
				Source:              "source 1",
				User:                "user 3 at source 1",
				WebResource:         "web resource 3",
				WebResourceCategory: "web resource category 3",
			},
		},
		{
			clientIP: net.ParseIP("10.200.0.2"),
			dstIP:    parseIPPanic("10.1.0.2"),
			expectedInfo: &types.CheckResult{
				Allowed: false,
				AccessGroup: &storage.AccessGroupV1{
					Metadata: storage.MetadataV1{
						APIVersion: storage.APIVersionV1Value,
						Kind:       storage.AccessGroupKind,
						Name:       "access group 2",
						ETag:       2,
					},
					Data: storage.AccessGroupDataV1{
						DefaultPolicy:           storage.DefaultAccessPolicyAllow,
						ExtraAccessDenyMessage:  "access group 2 access denied",
						OrderInAccessRulesList:  2,
						TotalGroupSpeedLimitBps: 100_002,
						UserGroupSpeedLimitBps:  1_002,
					},
				},
				Source:              "source 1",
				User:                "user 3 at source 1",
				WebResource:         "web resource 1",
				WebResourceCategory: "web resource category 1",
			},
		},
	}

	for _, test := range tests {
		// Check Access.
		gotRes, gotErr := ts.db.CheckAccessSIPDIP(test.clientIP, test.dstIP)

		if !ts.NoError(gotErr, "Must not return error") {
			ts.FailNow("Can not continue")
		}

		ts.Equal(test.expectedInfo, gotRes, "Unexpected result result")
	}
}

func TestCheckIPIP(t *testing.T) {
	suite.Run(t, &TestCheckIPIPSuite{})
}

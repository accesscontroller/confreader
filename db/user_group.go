package db

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"sort"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.etcd.io/bbolt"
)

func getUserGroupsBktTx(tx *bbolt.Tx) (*bbolt.Bucket, error) {
	var bkt = tx.Bucket(_UserGroupsBKT)
	if bkt == nil {
		return nil, fmt.Errorf("%w: User => Groups Bucket", ErrNoBucket)
	}

	return bkt, nil
}

type userWGroups struct {
	UserWSource storage.ResourceNameT

	Groups []storage.ResourceNameT
}

func convertGroupUsersRelations(relations []*storage.ExternalGroup2ExternalUserListRelationsV1) map[storage.ResourceNameT][][]byte {
	var result = make(map[storage.ResourceNameT][][]byte, 0)

	for _, r := range relations {
		var groupName = encodeGroupName(r.Data.ExternalUsersGroupsSource, r.Data.ExternalGroup)

		for uI := range r.Data.ExternalUsers {
			var uN = encodeUserName(r.Data.ExternalUsersGroupsSource, r.Data.ExternalUsers[uI])

			result[uN] = append(result[uN], groupName)
		}
	}

	return result
}

func encodeUserName(source, user storage.ResourceNameT) storage.ResourceNameT {
	return source + ";" + user
}

func encodeGroupName(source, group storage.ResourceNameT) []byte {
	return []byte(source + ";" + group)
}

func encodeUserGroups(groups [][]byte) ([]byte, error) {
	var bb = &bytes.Buffer{}

	if err := gob.NewEncoder(bb).Encode(groups); err != nil {
		return nil, err
	}

	return bb.Bytes(), nil
}

func decodeUserGroups(bts []byte) ([][]byte, error) {
	var result [][]byte

	if err := gob.NewDecoder(bytes.NewReader(bts)).Decode(&result); err != nil {
		return nil, err
	}

	return result, nil
}

func loadAllUserGroupsBkt(bkt *bbolt.Bucket) (map[storage.ResourceNameT][][]byte, error) {
	var result = make(map[storage.ResourceNameT][][]byte, bkt.Stats().KeyN)

	if err := bkt.ForEach(func(k, v []byte) error {
		// Decode.
		groups, err := decodeUserGroups(v)
		if err != nil {
			return err
		}

		result[storage.ResourceNameT(k)] = groups

		return nil
	}); err != nil {
		return nil, err
	}

	return result, nil
}

func compareSliceByte(v1, v2 []byte) bool {
	if len(v1) != len(v2) {
		return false
	}

	for i, b1 := range v1 {
		if b1 != v2[i] {
			return false
		}
	}

	return true
}

func compareGroupSets(set1, set2 [][]byte) bool {
	// Check that all set1 members are found in set2.
	for i := range set1 {
		var (
			isFound bool
			v1      = set1[i]
		)

		for j := range set2 {
			if compareSliceByte(set2[j], v1) {
				isFound = true

				break
			}
		}

		if !isFound {
			return false
		}
	}

	// Check that all set2 members are found in set1.
	for i := range set2 {
		var (
			isFound bool
			v2      = set2[i]
		)

		for j := range set1 {
			if compareSliceByte(set1[j], v2) {
				isFound = true

				break
			}
		}

		if !isFound {
			return false
		}
	}

	return true
}

func setExternalGroup2ExternalUserListRelationsV1Tx(
	tx *bbolt.Tx, relations []*storage.ExternalGroup2ExternalUserListRelationsV1) error {
	// Bucket.
	bkt, err := getUserGroupsBktTx(tx)
	if err != nil {
		return err
	}

	// Prepare reference data.
	var reference = convertGroupUsersRelations(relations)

	// Load DB Data.
	dbGroups, err := loadAllUserGroupsBkt(bkt)
	if err != nil {
		return err
	}

	// Delete.
	for dbK := range dbGroups {
		_, ok := reference[dbK]
		if ok {
			// Skip.
			continue
		}

		if err := bkt.Delete([]byte(dbK)); err != nil {
			return err
		}
	}

	// Create/Update.
	for refK, refGroups := range reference {
		dbV, ok := dbGroups[refK]
		if ok { // Possible update.
			// Check for update.
			if compareGroupSets(refGroups, dbV) {
				// Equal. No update required.
				continue
			}
		}

		// Create/Update.
		val, err := encodeUserGroups(refGroups)
		if err != nil {
			return err
		}

		if err := bkt.Put([]byte(refK), val); err != nil {
			return err
		}
	}

	return nil
}

// getUserGroupsTx loads all ExternalGroupV1s names for given ExternalUserV1.
// Names are returned as []byte slices to reduce string <=> []byte conversions.
func getUserGroupsTx(tx *bbolt.Tx, source, username storage.ResourceNameT) ([][]byte, error) {
	// Bucket.
	bkt, err := getUserGroupsBktTx(tx)
	if err != nil {
		return nil, err
	}

	// Load.
	var data = bkt.Get([]byte(encodeUserName(source, username)))
	if data == nil {
		return nil, fmt.Errorf("%w: User %s:%s", ErrNotFound, source, username)
	}

	// Decode.
	groups, err := decodeUserGroups(data)
	if err != nil {
		return nil, err
	}

	return groups, nil
}

// getUserAccessGroupV1sNamesTx returns all AccessGroupV1sNames to which given ExternalUserV1
// could be mapped through ExternalGroupV1s.
func getUserAccessGroupV1sNamesTx(tx *bbolt.Tx, source, username storage.ResourceNameT) ([][]byte, error) {
	// Load Groups.
	groups, err := getUserGroupsTx(tx, source, username)
	if err != nil {
		return nil, err
	}

	// Load AccessGroupV1s.
	accessGroups, err := getGroupsAccessGroupsTx(tx, groups...)
	if err != nil {
		return nil, err
	}

	return accessGroups, nil
}

// getUserFirstAccessGroupV1Tx loads and returns a first AccessGroupV1 for a given ExternalUserV1.
// The AccessGroupV1#Data#OrderInAccessRulesList defines the order of AccessGroupV1s.
func getUserFirstAccessGroupV1Tx(tx *bbolt.Tx, source, username storage.ResourceNameT) (*storage.AccessGroupV1, error) {
	// Lookup for User AccessGroupV1s.
	userAGroups, err := getUserAccessGroupV1sNamesTx(tx, source, username)
	if err != nil {
		return nil, err
	}

	// If no User groups - deny access with error.
	if len(userAGroups) == 0 {
		return nil, fmt.Errorf("%w: No AccessGroupV1s found for an ExternalUserV1 %s:%s",
			ErrNotFound, source, username)
	}

	// Load AccessGroupV1s properties.
	userAccessGroups, err := getAccessGroupPropertiesTx(tx, userAGroups...)
	if err != nil {
		return nil, err
	}

	// Sort.
	sort.Slice(userAccessGroups, func(i, j int) bool {
		return userAccessGroups[i].Data.OrderInAccessRulesList < userAccessGroups[j].Data.OrderInAccessRulesList
	})

	// Get first match.
	return userAccessGroups[0], nil
}

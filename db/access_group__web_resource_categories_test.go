package db

import (
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.etcd.io/bbolt"
)

type TestSetAccessGroup2WebResourceCategoryListRelationsV1TxSuite struct {
	db *DB

	suite.Suite
}

func (ts *TestSetAccessGroup2WebResourceCategoryListRelationsV1TxSuite) SetupTest() {
	// Faker config.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNowf("Can not init Faker", "Error: %s", err)
	}

	db, err := initDBTest()
	if err != nil {
		ts.FailNow("Can not open DB", err)
	}

	ts.db = db

	// Pre-create some data.
	if err := ts.db.db.Batch(func(tx *bbolt.Tx) error {
		bkt, err := getWebResourceCategoryAccessGroupBktTx(tx)
		if err != nil {
			return err
		}

		// Put Data.
		var data = map[storage.ResourceNameT][][]byte{
			"category 1-1": {[]byte("access group 1"), []byte("access group 2")},
			"category 2-1": {[]byte("access group 1")},
			"category 3-1": {[]byte("access group 1")},
			"category 3-2": {[]byte("access group 2")},
		}

		for k, v := range data {
			bts, err := encodeAccessGroupV1NamesSlice(v)
			if err != nil {
				return err
			}

			if err := bkt.Put([]byte(k), bts); err != nil {
				return err
			}
		}

		return nil
	}); err != nil {
		ts.FailNowf("Can not pre-create required data", "Error: %s", err)
	}
}

func (ts *TestSetAccessGroup2WebResourceCategoryListRelationsV1TxSuite) TearDownTest() {
	if err := ts.db.db.Close(); err != nil {
		ts.FailNow("Can not close DB", err)
	}
}

func (ts *TestSetAccessGroup2WebResourceCategoryListRelationsV1TxSuite) TestSetSuccess() {
	var tests []*struct {
		Relations []*storage.AccessGroup2WebResourceCategoryListRelationsV1
	}

	// Faker.
	if err := faker.FakeData(&tests); err != nil {
		ts.FailNowf("Can not Fake Data", "Error: %s", err)
	}

	for _, test := range tests {
		// Add existing data to check updates.
		test.Relations = append(test.Relations, &storage.AccessGroup2WebResourceCategoryListRelationsV1{
			Data: storage.AccessGroup2WebResourceCategoryListRelationsDataV1{
				AccessGroup: "access group 1",
				Categories: []storage.ResourceNameT{
					"category 1-1",
					"new category 4-1",
				},
			},
		})

		// Set.
		err := ts.db.SetAccessGroup2WebResourceCategoryListRelationsV1(test.Relations)

		if !ts.NoError(err, "Must not return error") {
			ts.FailNowf("Got error - can not continue", "Test: %+v, Error: %s", test, err)
		}

		// Load and compare.
		// Transaction.
		if err := ts.db.db.Batch(func(tx *bbolt.Tx) error {
			// Load.
			bkt, err := getWebResourceCategoryAccessGroupBktTx(tx)
			if err != nil {
				ts.FailNowf("Can not get Bucket", "Error: %s", err)
			}

			var dbData = make(map[storage.ResourceNameT][][]byte, bkt.Stats().KeyN)

			if err := bkt.ForEach(func(k, v []byte) error {
				// Decode.
				accessGroups, err := decodeAccessGroupV1NamesSlice(v)
				if err != nil {
					return err
				}

				dbData[storage.ResourceNameT(k)] = accessGroups

				return nil
			}); err != nil {
				ts.FailNowf("Can not load data from DB", "Error: %s", err)
			}

			// Convert relations.
			var expRelations = convertWebResourcesToAccessGroups(test.Relations)

			ts.Equal(expRelations, dbData, "Unexpected result")

			return nil
		}); err != nil {
			ts.FailNowf("Can not open transaction", "Error: %s", err)
		}
	}
}

func TestAccessGroupWebResourceCategories(t *testing.T) {
	suite.Run(t, &TestSetAccessGroup2WebResourceCategoryListRelationsV1TxSuite{})
}

type TestGetWRAccessGroupBktTxSuite struct {
	suite.Suite
}

func (ts *TestGetWRAccessGroupBktTxSuite) TestGetSuccess() {
	db, err := initDBTest()

	if !ts.NoError(err, "Must not return error") {
		ts.FailNowf("Got error - can not continue", "Error: %s", err)
	}

	// Get.
	err = db.db.Batch(func(tx *bbolt.Tx) error {
		bkt, err := getWebResourceCategoryAccessGroupBktTx(tx)
		if err != nil {
			return err
		}

		ts.NotNil(bkt, "Must not return nil Bucket")

		return nil
	})

	ts.NoError(err, "Must not return error")
}

func (ts *TestGetWRAccessGroupBktTxSuite) TestGetError() {
	db, err := initDBTestNoBuckets()

	if !ts.NoError(err, "Must not return error") {
		ts.FailNowf("Got error - can not continue", "Error: %s", err)
	}

	// Get.
	err = db.db.Batch(func(tx *bbolt.Tx) error {
		bkt, err := getWebResourceCategoryAccessGroupBktTx(tx)
		if err != nil {
			return err
		}

		ts.Nil(bkt, "Must return nil Bucket")

		return nil
	})

	ts.Error(err, "Must return error")
}

func TestGetWRAccessGroupBktTx(t *testing.T) {
	suite.Run(t, &TestGetWRAccessGroupBktTxSuite{})
}

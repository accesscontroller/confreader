package db

import (
	"net"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

func encodeIP(addr net.IP) _IP {
	var res _IP

	copy(res[:], addr)

	return res
}

func decodeIP(addr _IP) net.IP {
	return net.IP(addr[:])
}

func computeAccessPolicyResult(group *storage.AccessGroupV1, webResourceAccessGroupNames [][]byte) bool {
	var groupName = []byte(group.Metadata.Name)

	for _, wG := range webResourceAccessGroupNames {
		if compareAccessGroupV1Names(wG, groupName) {
			// WebResourceV1 is a default policy exception.
			return !group.Data.DefaultPolicy.IsAccessAllowed()
		}
	}

	// No exception, policy is used as is.
	return group.Data.DefaultPolicy.IsAccessAllowed()
}

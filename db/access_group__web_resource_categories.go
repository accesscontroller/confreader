package db

import (
	"bytes"
	"encoding/gob"
	"fmt"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.etcd.io/bbolt"
)

func getWebResourceCategoryAccessGroupBktTx(tx *bbolt.Tx) (*bbolt.Bucket, error) {
	var bkt = tx.Bucket(_WResCatAccGrpsBKT)
	if bkt == nil {
		return nil, fmt.Errorf("%w: WebResourceV1 => AccessGroupV1 Bucket", ErrNoBucket)
	}

	return bkt, nil
}

func encodeAccessGroupV1NamesSlice(groups [][]byte) ([]byte, error) {
	var bb bytes.Buffer

	if err := gob.NewEncoder(&bb).Encode(groups); err != nil {
		return nil, err
	}

	return bb.Bytes(), nil
}

func decodeAccessGroupV1NamesSlice(bts []byte) ([][]byte, error) {
	var result [][]byte

	if err := gob.NewDecoder(bytes.NewReader(bts)).Decode(&result); err != nil {
		return nil, err
	}

	return result, nil
}

func convertWebResourcesToAccessGroups(
	relations []*storage.AccessGroup2WebResourceCategoryListRelationsV1) map[storage.ResourceNameT][][]byte {
	var result = make(map[storage.ResourceNameT][][]byte)

	for _, pr := range relations {
		for j := range pr.Data.Categories {
			var cat = pr.Data.Categories[j]

			result[cat] = append(result[cat], []byte(pr.Data.AccessGroup))
		}
	}

	return result
}

func loadWebResourcesToAccessGroupsBkt(bkt *bbolt.Bucket) (map[storage.ResourceNameT][][]byte, error) {
	var result = make(map[storage.ResourceNameT][][]byte, bkt.Stats().KeyN)

	if err := bkt.ForEach(func(k, v []byte) error {
		// Decode.
		accessGroups, err := decodeAccessGroupV1NamesSlice(v)
		if err != nil {
			return err
		}

		result[storage.ResourceNameT(k)] = accessGroups

		return nil
	}); err != nil {
		return nil, err
	}

	return result, nil
}

func compareAccessGroupV1Names(n1, n2 []byte) bool {
	if len(n1) != len(n2) {
		return false
	}

	for i, cn1 := range n1 {
		if cn1 != n2[i] {
			return false
		}
	}

	return true
}

func compareAccessGroupV1sNames(set1, set2 [][]byte) bool {
	// Check that all values from set1 exist in set2.
	for _, v1 := range set1 {
		var isFound bool

		for j := range set2 {
			if compareAccessGroupV1Names(v1, set2[j]) {
				isFound = true

				break
			}
		}

		if !isFound {
			return false
		}
	}

	// Check that all values from set2 exist in set1.
	for _, v2 := range set2 {
		var isFound bool

		for j := range set1 {
			if compareAccessGroupV1Names(v2, set1[j]) {
				isFound = true

				break
			}
		}

		if !isFound {
			return false
		}
	}

	return true
}

func setAccessGroup2WebResourceCategoryListRelationsV1Tx( // nolint:dupl
	tx *bbolt.Tx, relations []*storage.AccessGroup2WebResourceCategoryListRelationsV1) error {
	// Get Bucket.
	bkt, err := getWebResourceCategoryAccessGroupBktTx(tx)
	if err != nil {
		return err
	}

	// Convert Relations.
	var reference = convertWebResourcesToAccessGroups(relations)

	// Load from DB.
	dbCats, err := loadWebResourcesToAccessGroupsBkt(bkt)
	if err != nil {
		return err
	}

	// Delete.
	for dbK := range dbCats {
		_, ok := reference[dbK]
		if ok {
			// Exists. Skip.
			continue
		}

		// Delete.
		if err := bkt.Delete([]byte(dbK)); err != nil {
			return err
		}
	}

	// Create.
	for refWebResource, refAccessGroups := range reference {
		dbGroups, ok := dbCats[refWebResource]
		if ok {
			// Check for update.
			if compareAccessGroupV1sNames(dbGroups, refAccessGroups) {
				continue
			}
		}

		// Create/Update.
		data, err := encodeAccessGroupV1NamesSlice(refAccessGroups)
		if err != nil {
			return err
		}

		if err := bkt.Put([]byte(refWebResource), data); err != nil {
			return err
		}
	}

	return nil
}

// getWebResourceCategoryV1AccessGroupV1sNamesTx loads and returns all AccessGroupV1s Names
// in which the given WebResourceCategoryV1 exists as DefaultPolicyException.
func getWebResourceCategoryV1AccessGroupV1sNamesTx(tx *bbolt.Tx, category []byte) ([][]byte, error) {
	// Bucket.
	bkt, err := getWebResourceCategoryAccessGroupBktTx(tx)
	if err != nil {
		return nil, err
	}

	// Load data.
	raw := bkt.Get(category)
	if raw == nil {
		return [][]byte{}, nil
	}

	// Decode.
	names, err := decodeAccessGroupV1NamesSlice(raw)
	if err != nil {
		return nil, err
	}

	return names, nil
}

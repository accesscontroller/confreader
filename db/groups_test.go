package db

import (
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.etcd.io/bbolt"
)

type TestSetExternalGroupV1sTxSuite struct {
	db *DB

	suite.Suite
}

func (ts *TestSetExternalGroupV1sTxSuite) SetupTest() {
	// Faker config.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNowf("Can not init Faker", "Error: %s", err)
	}

	db, err := initDBTest()
	if err != nil {
		ts.FailNow("Can not open DB", err)
	}

	ts.db = db

	// Pre-create some data.
	if err := ts.db.db.Batch(func(tx *bbolt.Tx) error {
		// Put ExternalGroupV1s.
		var data = []*storage.ExternalGroupV1{
			{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       123,
					Kind:       storage.AccessGroupKind,
					Name:       "external group 1",
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 1",
					},
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 1",
				},
			},
			{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       124,
					Kind:       storage.AccessGroupKind,
					Name:       "external group 2",
					ExternalSource: &storage.ExternalSourceInfo{
						SourceName: "source 2",
					},
				},
				Data: storage.ExternalGroupDataV1{
					AccessGroupName: "access group 1",
				},
			},
		}

		bkt, err := getGroupAccessGroupBktTx(tx)
		if err != nil {
			return err
		}

		for _, v := range data {
			if err := bkt.Put(
				[]byte(v.Metadata.ExternalSource.SourceName+";"+v.Metadata.Name),
				[]byte(v.Data.AccessGroupName)); err != nil {
				return err
			}
		}

		return nil
	}); err != nil {
		ts.FailNowf("Can not pre-create required data", "Error: %s", err)
	}
}

func (ts *TestSetExternalGroupV1sTxSuite) TearDownTest() {
	if err := ts.db.db.Close(); err != nil {
		ts.FailNow("Can not close DB", err)
	}
}

func (ts *TestSetExternalGroupV1sTxSuite) TestSetSuccess() {
	var test = struct {
		Groups []storage.ExternalGroupV1
	}{}

	// Fake.
	if err := faker.FakeData(&test); err != nil {
		ts.FailNowf("Can not FakeData", "Error: %s", err)
	}

	// Set Random ExternalSourceV1s.
	for i := range test.Groups {
		var source storage.ResourceNameT

		if err := faker.FakeData(&source); err != nil {
			ts.FailNowf("Can not FakeData", "Error: %s", err)
		}

		test.Groups[i].Metadata.ExternalSource = &storage.ExternalSourceInfo{
			SourceName: source,
			Kind:       storage.ExternalUsersGroupsSourceKind,
		}
	}

	// Add existing data to check for update.
	test.Groups = append(test.Groups, storage.ExternalGroupV1{
		Metadata: storage.MetadataV1{
			APIVersion: storage.APIVersionV1Value,
			ETag:       124,
			Kind:       storage.AccessGroupKind,
			Name:       "external group 2",
			ExternalSource: &storage.ExternalSourceInfo{
				SourceName: "source 2",
			},
		},
		Data: storage.ExternalGroupDataV1{
			AccessGroupName: "access group 1",
		},
	})

	// Test.
	err := ts.db.SetExternalGroupV1s(test.Groups)

	if !ts.NoError(err, "Must not return Set error") {
		ts.FailNow("Got error - can not continue")
	}

	var (
		expectedGroups = convertGroupAccessGroup(test.Groups)
		dbGroups       map[storage.ResourceNameT]storage.ResourceNameT
	)

	// Check DB data.
	err = ts.db.db.Batch(func(tx *bbolt.Tx) error {
		bkt, err := getGroupAccessGroupBktTx(tx)
		if err != nil {
			return err
		}

		grs, err := loadAllGroupAccessGroupsBkt(bkt)
		if err != nil {
			return err
		}

		dbGroups = grs

		return nil
	})

	ts.Equal(expectedGroups, dbGroups, "Unexpected External Groups => Access Groups loaded from DB")
}

func TestSetExternalGroupV1sTx(t *testing.T) {
	suite.Run(t, &TestSetExternalGroupV1sTxSuite{})
}

package db

import (
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.etcd.io/bbolt"
)

type TestAccessGroupPropertiesSuite struct {
	db *DB

	suite.Suite
}

func (ts *TestAccessGroupPropertiesSuite) SetupTest() {
	// Faker config.
	if err := fakerCustomGenerator(); err != nil {
		ts.FailNowf("Can not init Faker", "Error: %s", err)
	}

	db, err := initDBTest()
	if err != nil {
		ts.FailNow("Can not open DB", err)
	}

	ts.db = db

	// Pre-create some data.
	if err := ts.db.db.Batch(func(tx *bbolt.Tx) error {
		// Put AccessGroupV1s.
		var data = []*storage.AccessGroupV1{
			{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       123,
					Kind:       storage.AccessGroupKind,
					Name:       "access group 1",
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyDeny,
					ExtraAccessDenyMessage:  "message 1",
					OrderInAccessRulesList:  1001,
					TotalGroupSpeedLimitBps: 111112,
					UserGroupSpeedLimitBps:  1113,
				},
			},
			{
				Metadata: storage.MetadataV1{
					APIVersion: storage.APIVersionV1Value,
					ETag:       124,
					Kind:       storage.AccessGroupKind,
					Name:       "access group 2",
				},
				Data: storage.AccessGroupDataV1{
					DefaultPolicy:           storage.DefaultAccessPolicyDeny,
					ExtraAccessDenyMessage:  "message 2",
					OrderInAccessRulesList:  1002,
					TotalGroupSpeedLimitBps: 111113,
					UserGroupSpeedLimitBps:  1114,
				},
			},
		}

		bkt, err := getAccessGroupBucketTx(tx)
		if err != nil {
			return err
		}

		for _, v := range data {
			bts, err := encodeAccessGroupProperties(v)
			if err != nil {
				return err
			}

			if err := bkt.Put([]byte(v.Metadata.Name), bts); err != nil {
				return err
			}
		}

		return nil
	}); err != nil {
		ts.FailNowf("Can not pre-create required data", "Error: %s", err)
	}
}

func (ts *TestAccessGroupPropertiesSuite) TearDownTest() {
	if err := ts.db.db.Close(); err != nil {
		ts.FailNow("Can not close DB", err)
	}
}

func (ts *TestAccessGroupPropertiesSuite) TestSetSuccess() {
	var test = struct {
		Groups []storage.AccessGroupV1
	}{}

	// Fake data.
	if err := faker.FakeData(&test); err != nil {
		ts.FailNowf("Can not FakeData", "Error: %s", err)
	}

	// Add existing in DB data.
	test.Groups = append(test.Groups, storage.AccessGroupV1{
		Metadata: storage.MetadataV1{
			APIVersion: storage.APIVersionV1Value,
			ETag:       124,
			Kind:       storage.AccessGroupKind,
			Name:       "access group 2",
		},
		Data: storage.AccessGroupDataV1{
			DefaultPolicy:           storage.DefaultAccessPolicyDeny,
			ExtraAccessDenyMessage:  "message 2",
			OrderInAccessRulesList:  1002,
			TotalGroupSpeedLimitBps: 111113,
			UserGroupSpeedLimitBps:  1114,
		},
	})

	// Test.
	err := ts.db.SetAccessGroupV1s(test.Groups)

	if !ts.NoError(err, "Must not return Set error") {
		ts.FailNow("Got error - can not continue")
	}

	var (
		expectedGroups = convertAccessGroupV1stoMap(test.Groups)
		dbGroups       map[storage.ResourceNameT]*storage.AccessGroupV1
	)

	// Check DB data.
	err = ts.db.db.Batch(func(tx *bbolt.Tx) error {
		grs, err := loadAllAccessGroupV1sTx(tx)
		if err != nil {
			return err
		}

		dbGroups = grs

		return nil
	})

	ts.Equal(expectedGroups, dbGroups, "Unexpected Access Groups loaded from DB")
}

func TestAccessGroupProperties(t *testing.T) {
	suite.Run(t, &TestAccessGroupPropertiesSuite{})
}

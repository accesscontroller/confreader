package db

import (
	"bytes"
	"encoding/gob"
	"fmt"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.etcd.io/bbolt"
)

func getAccessGroupBucketTx(tx *bbolt.Tx) (*bbolt.Bucket, error) {
	bkt := tx.Bucket(_AccGrPropBKT)
	if bkt == nil {
		return nil, fmt.Errorf("%w: AccessGroupV1 => Properties Bucket", ErrNoBucket)
	}

	return bkt, nil
}

func encodeAccessGroupProperties(group *storage.AccessGroupV1) ([]byte, error) {
	var bb bytes.Buffer

	if err := gob.NewEncoder(&bb).Encode(group); err != nil {
		return nil, err
	}

	return bb.Bytes(), nil
}

func decodeAccessGroupProperties(bts []byte) (*storage.AccessGroupV1, error) {
	var group storage.AccessGroupV1

	if err := gob.NewDecoder(bytes.NewReader(bts)).Decode(&group); err != nil {
		return nil, err
	}

	return &group, nil
}

// loadAllAccessGroupV1sTx loads all AccessGroupV1s.
func loadAllAccessGroupV1sTx(tx *bbolt.Tx) (map[storage.ResourceNameT]*storage.AccessGroupV1, error) {
	// Get Bucket.
	bkt, err := getAccessGroupBucketTx(tx)
	if err != nil {
		return nil, err
	}

	var result = make(map[storage.ResourceNameT]*storage.AccessGroupV1, bkt.Stats().KeyN)

	// Load.
	if err := bkt.ForEach(func(k, v []byte) error {
		group, err := decodeAccessGroupProperties(v)
		if err != nil {
			return err
		}

		result[group.Metadata.Name] = group

		return nil
	}); err != nil {
		return nil, err
	}

	return result, nil
}

// createAccessGroupV1Tx stores one AccessGroupV1.
func createAccessGroupV1Tx(bkt *bbolt.Bucket, group *storage.AccessGroupV1) error {
	bts, err := encodeAccessGroupProperties(group)
	if err != nil {
		return err
	}

	return bkt.Put([]byte(group.Metadata.Name), bts)
}

// convertAccessGroupV1stoMap converts AccessGroupV1s slice representation.
func convertAccessGroupV1stoMap(groups []storage.AccessGroupV1) map[storage.ResourceNameT]*storage.AccessGroupV1 {
	var result = make(map[storage.ResourceNameT]*storage.AccessGroupV1, len(groups))

	for i := range groups {
		result[groups[i].Metadata.Name] = &groups[i]
	}

	return result
}

// setAccessGroupV1sPropertiesTx performs AccessGroupV1s properties synchronization.
func setAccessGroupV1sPropertiesTx(tx *bbolt.Tx, groups []storage.AccessGroupV1) error {
	// Get Bucket.
	bkt, err := getAccessGroupBucketTx(tx)
	if err != nil {
		return err
	}

	// Retrieve all AccessGroupV1s.
	dbGroups, err := loadAllAccessGroupV1sTx(tx)
	if err != nil {
		return err
	}

	// Prepare reference groups.
	var refGroups = convertAccessGroupV1stoMap(groups)

	// Delete.
	for dbName := range dbGroups {
		_, ok := refGroups[dbName]
		if !ok {
			if err := bkt.Delete([]byte(dbName)); err != nil {
				return err
			}
		}
	}

	// Update/Create.
	for _, refGroup := range refGroups {
		// Add.
		if err := createAccessGroupV1Tx(bkt, refGroup); err != nil {
			return err
		}
	}

	return nil
}

// getAccessGroupsPropertiesTx loads AccessGroupV1s.
func getAccessGroupPropertiesTx(tx *bbolt.Tx, names ...[]byte) ([]*storage.AccessGroupV1, error) {
	// Bucket.
	bkt, err := getAccessGroupBucketTx(tx)
	if err != nil {
		return nil, err
	}

	// Load.
	var groups []*storage.AccessGroupV1

	for _, v := range names {
		// Load.
		var bts = bkt.Get(v)
		if bts == nil {
			return nil, fmt.Errorf("%w: AccessGroupV1 %s", ErrNotFound, string(v))
		}

		// Decode.
		gr, err := decodeAccessGroupProperties(bts)
		if err != nil {
			return nil, err
		}

		groups = append(groups, gr)
	}

	return groups, nil
}

func computeAccessGroupV1NamesIntersect(nSet1, nSet2 [][]byte) [][]byte {
	var result [][]byte

	for _, n1 := range nSet1 {
		for _, n2 := range nSet2 {
			if compareAccessGroupV1Names(n1, n2) {
				result = append(result, n1)

				break // Found once - don't need to lookup for further occurrences.
			}
		}
	}

	return result
}

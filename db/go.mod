module gitlab.com/accesscontroller/confreader/db

go 1.15

require (
	github.com/bxcodec/faker/v3 v3.5.0
	github.com/kr/text v0.2.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/stretchr/testify v1.6.1
	gitlab.com/accesscontroller/accesscontroller/controller/storage v0.0.0-20200913103725-48995d0f8362
	gitlab.com/accesscontroller/confreader/types v0.0.0-20200816053952-fa7ce30259cd
	go.etcd.io/bbolt v1.3.5
	golang.org/x/sys v0.0.0-20200918174421-af09f7315aff // indirect
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
)

package db

import (
	"net/url"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/confreader/types"
	"go.etcd.io/bbolt"
)

func checkAccessUserURLTx(tx *bbolt.Tx, source, username storage.ResourceNameT, u *url.URL) (*types.CheckResult, error) {
	// Find a first AccessGroupV1 for given User.
	userAccessGroup, err := getUserFirstAccessGroupV1Tx(tx, source, username)
	if err != nil {
		return nil, err
	}

	// Lookup for URL Information.
	urlInfo, err := getURLAccessGroupV1sNamesTx(tx, u)
	if err != nil {
		return nil, err
	}

	return &types.CheckResult{
		Allowed:             computeAccessPolicyResult(userAccessGroup, urlInfo.accessGroups),
		AccessGroup:         userAccessGroup,
		Source:              source,
		User:                username,
		WebResource:         urlInfo.resource,
		WebResourceCategory: urlInfo.category,
	}, nil
}

package db

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"net"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.etcd.io/bbolt"
)

func getSessionsBktTx(tx *bbolt.Tx) (*bbolt.Bucket, error) {
	var bkt = tx.Bucket(_IPSessionBKT)
	if bkt == nil {
		return nil, fmt.Errorf("%w: IP => Session Bucket", ErrNoBucket)
	}

	return bkt, nil
}

type userSource struct {
	User   storage.ResourceNameT
	Source storage.ResourceNameT
}

func encodeSession(source, user storage.ResourceNameT) ([]byte, error) {
	var bb = &bytes.Buffer{}

	if err := gob.NewEncoder(bb).Encode(userSource{Source: source, User: user}); err != nil {
		return nil, fmt.Errorf("can not encode ExternalUserV1 ID: %w", err)
	}

	return bb.Bytes(), nil
}

func decodeSession(data []byte) (*userSource, error) {
	var us userSource

	if err := gob.NewDecoder(bytes.NewReader(data)).Decode(&us); err != nil {
		return nil, fmt.Errorf("can not decode ExternalUserV1 ID: %w", err)
	}

	return &us, nil
}

func convertIPsToSessions(sessions []storage.ExternalUserSessionV1) (map[_IP][]byte, error) {
	var result = make(map[_IP][]byte, len(sessions))

	for i := range sessions {
		ps := &sessions[i]

		var k = encodeIP(ps.Data.IPAddress)

		b, err := encodeSession(ps.Data.ExternalUsersGroupsSourceName, ps.Data.ExternalUserName)
		if err != nil {
			return nil, err
		}

		result[k] = b
	}

	return result, nil
}

func loadAllSessions(bkt *bbolt.Bucket) (map[_IP][]byte, error) {
	var result = make(map[_IP][]byte, bkt.Stats().KeyN)

	if err := bkt.ForEach(func(k, v []byte) error {
		var _k = encodeIP(k)

		result[_k] = v

		return nil
	}); err != nil {
		return nil, err
	}

	return result, nil
}

func setExternalUserSessionV1sTx(tx *bbolt.Tx, sessions []storage.ExternalUserSessionV1) error {
	// Get Bucket.
	bkt, err := getSessionsBktTx(tx)
	if err != nil {
		return err
	}

	// Load DB sessions.
	dbSessions, err := loadAllSessions(bkt)
	if err != nil {
		return err
	}

	// Convert reference data.
	reference, err := convertIPsToSessions(sessions)
	if err != nil {
		return err
	}

	// Delete.
	for dbK := range dbSessions {
		_, ok := reference[dbK]
		if ok {
			continue
		}

		if err := bkt.Delete(dbK[:]); err != nil {
			return err
		}
	}

	// Update/Create.
	for refK, refS := range reference {
		dbS, ok := dbSessions[refK]
		if ok && compareSliceByte(dbS, refS) { // Do nothing.
			continue
		}

		// Create/Update.
		if err := bkt.Put(refK[:], refS); err != nil {
			return err
		}
	}

	return nil
}

// getIPUserTx loads a Session by IP address.
func getIPUserTx(tx *bbolt.Tx, addr net.IP) (*userSource, error) {
	// Get Bucket.
	bkt, err := getSessionsBktTx(tx)
	if err != nil {
		return nil, err
	}

	// Get User Info.
	raw := bkt.Get(addr)
	if raw == nil {
		return nil, fmt.Errorf("%w: Can not find a Session with IP %q", ErrNotFound, addr)
	}

	// Load Session.
	s, err := decodeSession(raw)
	if err != nil {
		return nil, err
	}

	return s, nil
}

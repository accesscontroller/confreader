package db

import (
	"errors"
	"fmt"
	"net"
	"net/url"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/confreader/types"
	"go.etcd.io/bbolt"
)

// nolint:noglobals,gochecknoglobals
var (
	_IPSessionBKT        = []byte("IP2Session")
	_UserGroupsBKT       = []byte("User2Groups")
	_GroupAccessGroupBKT = []byte("Group2AccessGroups")

	_URLWResBKT        = []byte("URL2WResource")
	_IPWResBKT         = []byte("IP2WResource")
	_DNWResBKT         = []byte("DomainName2WResource")
	_WResWResCatBKT    = []byte("WResource2WResourceCategory")
	_WResCatAccGrpsBKT = []byte("WResCategory2AccessGroup")

	_AccGrPropBKT = []byte("AccessGroup2Props")

	_Source2GroupsBKTBKT = []byte("Source2Groups")
	_Source2UsersBKT     = []byte("Source2Users")
)

var (
	ErrNotFound = errors.New("key was not found")
	ErrNoBucket = errors.New("no bucket found")
)

// DB allows saving/loading API server data.
type DB struct {
	db *bbolt.DB
}

// New opens local database and creates it if does not exist.
func New(filePath string) (*DB, error) {
	db, err := bbolt.Open(filePath, 0600, nil)
	if err != nil {
		return nil, err
	}

	var resDB = DB{db: db}

	// Create required buckets.
	if err := db.Update(func(tx *bbolt.Tx) error {
		var err error

		_, err = tx.CreateBucketIfNotExists(_IPSessionBKT)
		if err != nil {
			return err
		}

		_, err = tx.CreateBucketIfNotExists(_UserGroupsBKT)
		if err != nil {
			return err
		}

		_, err = tx.CreateBucketIfNotExists(_GroupAccessGroupBKT)
		if err != nil {
			return err
		}

		_, err = tx.CreateBucketIfNotExists(_URLWResBKT)
		if err != nil {
			return err
		}

		_, err = tx.CreateBucketIfNotExists(_IPWResBKT)
		if err != nil {
			return err
		}

		_, err = tx.CreateBucketIfNotExists(_DNWResBKT)
		if err != nil {
			return err
		}

		_, err = tx.CreateBucketIfNotExists(_WResWResCatBKT)
		if err != nil {
			return err
		}

		_, err = tx.CreateBucketIfNotExists(_WResCatAccGrpsBKT)
		if err != nil {
			return err
		}

		_, err = tx.CreateBucketIfNotExists(_AccGrPropBKT)
		if err != nil {
			return err
		}

		_, err = tx.CreateBucketIfNotExists(_Source2GroupsBKTBKT)
		if err != nil {
			return err
		}

		_, err = tx.CreateBucketIfNotExists(_Source2UsersBKT)
		if err != nil {
			return err
		}

		return nil
	}); err != nil {
		return nil, err
	}

	return &resDB, nil
}

// SetAccessGroupV1s updates internal DB with AccessGroupV1s data.
func (d *DB) SetAccessGroupV1s(groups []storage.AccessGroupV1) error {
	if err := d.db.Batch(func(tx *bbolt.Tx) error {
		// Update AccessGroupV1 properties.
		if err := setAccessGroupV1sPropertiesTx(tx, groups); err != nil {
			return fmt.Errorf("error SetAccessGroupV1s: %w", err)
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}

// SetWebResourcesV1Tx updates internal DB with WebResourceV1s data.
func (d *DB) SetWebResourcesV1Tx(resources []storage.WebResourceV1) error {
	if err := d.db.Batch(func(tx *bbolt.Tx) error {
		if err := setWebResourcesV1Tx(tx, resources); err != nil {
			return err
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}

// SetAccessGroup2WebResourceCategoryListRelationsV1 updates internal DB with AccessGroup2WebResourceCategoryListRelationsV1s data.
func (d *DB) SetAccessGroup2WebResourceCategoryListRelationsV1(
	relations []*storage.AccessGroup2WebResourceCategoryListRelationsV1) error {
	if err := d.db.Batch(func(tx *bbolt.Tx) error {
		if err := setAccessGroup2WebResourceCategoryListRelationsV1Tx(tx, relations); err != nil {
			return err
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}

// SetExternalUserSessionV1s updates internal DB with ExternalUserSessionV1 data.
func (d *DB) SetExternalUserSessionV1s(sessions []storage.ExternalUserSessionV1) error {
	if err := d.db.Batch(func(tx *bbolt.Tx) error {
		if err := setExternalUserSessionV1sTx(tx, sessions); err != nil {
			return err
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}

// SetExternalGroup2ExternalUserListRelationsV1 updates internal DB with ExternalGroup2ExternalUserListRelationsV1 data.
func (d *DB) SetExternalGroup2ExternalUserListRelationsV1(relations []*storage.ExternalGroup2ExternalUserListRelationsV1) error {
	if err := d.db.Batch(func(tx *bbolt.Tx) error {
		if err := setExternalGroup2ExternalUserListRelationsV1Tx(tx, relations); err != nil {
			return err
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}

// SetExternalGroupV1s updates internal DB with ExternalGroupV1s data.
func (d *DB) SetExternalGroupV1s(groups []storage.ExternalGroupV1) error {
	if err := d.db.Batch(func(tx *bbolt.Tx) error {
		if err := setExternalGroupV1sTx(tx, groups); err != nil {
			return err
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}

// CheckAccessUserURL performs access permissions check for a User to a URL.
func (d *DB) CheckAccessUserURL(source, username storage.ResourceNameT, u *url.URL) (*types.CheckResult, error) {
	var ch *types.CheckResult

	if err := d.db.Batch(func(tx *bbolt.Tx) error {
		res, err := checkAccessUserURLTx(tx, source, username, u)
		if err != nil {
			return err
		}

		ch = res

		return nil
	}); err != nil {
		return nil, err
	}

	return ch, nil
}

// CheckAccessUserDomain performs access permissions check for a User to a Domain.
func (d *DB) CheckAccessUserDomain(source, username storage.ResourceNameT, domain storage.WebResourceDomainT) (*types.CheckResult, error) {
	var ch *types.CheckResult

	if err := d.db.Batch(func(tx *bbolt.Tx) error {
		res, err := checkAccessUserDomainTx(tx, source, username, domain)
		if err != nil {
			return err
		}

		ch = res

		return nil
	}); err != nil {
		return nil, err
	}

	return ch, nil
}

// CheckAccessUserIP performs access permissions check for a User to an IP.
func (d *DB) CheckAccessUserIP(source, username storage.ResourceNameT, addr net.IP) (*types.CheckResult, error) {
	var ch *types.CheckResult

	if err := d.db.Batch(func(tx *bbolt.Tx) error {
		res, err := checkAccessUserIPTx(tx, source, username, addr)
		if err != nil {
			return err
		}

		ch = res

		return nil
	}); err != nil {
		return nil, err
	}

	return ch, nil
}

// CheckAccessSIPURL checks a source IP access to a URL.
func (d *DB) CheckAccessSIPURL(srcIP net.IP, u *url.URL) (*types.CheckResult, error) {
	var ch *types.CheckResult

	if err := d.db.Batch(func(tx *bbolt.Tx) error {
		usr, err := getIPUserTx(tx, srcIP)
		if err != nil {
			return err
		}

		res, err := checkAccessUserURLTx(tx, usr.Source, usr.User, u)
		if err != nil {
			return err
		}

		ch = res

		return nil
	}); err != nil {
		return nil, err
	}

	return ch, nil
}

// CheckAccessSIPDomain checks a source IP access to a Domain.
func (d *DB) CheckAccessSIPDomain(srcIP net.IP, domain storage.WebResourceDomainT) (*types.CheckResult, error) {
	var ch *types.CheckResult

	if err := d.db.Batch(func(tx *bbolt.Tx) error {
		usr, err := getIPUserTx(tx, srcIP)
		if err != nil {
			return err
		}

		res, err := checkAccessUserDomainTx(tx, usr.Source, usr.User, domain)
		if err != nil {
			return err
		}

		ch = res

		return nil
	}); err != nil {
		return nil, err
	}

	return ch, nil
}

// CheckAccessSIPDIP checks a source IP access to an IP.
func (d *DB) CheckAccessSIPDIP(srcIP, dstIP net.IP) (*types.CheckResult, error) {
	var ch *types.CheckResult

	if err := d.db.Batch(func(tx *bbolt.Tx) error {
		usr, err := getIPUserTx(tx, srcIP)
		if err != nil {
			return err
		}

		res, err := checkAccessUserIPTx(tx, usr.Source, usr.User, dstIP)
		if err != nil {
			return err
		}

		ch = res

		return nil
	}); err != nil {
		return nil, err
	}

	return ch, nil
}

package db

import (
	"fmt"
	"net"
	"net/url"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.etcd.io/bbolt"
)

func getIPsBktTx(tx *bbolt.Tx) (*bbolt.Bucket, error) {
	bkt := tx.Bucket(_IPWResBKT)
	if bkt == nil {
		return nil, fmt.Errorf("%w: IP => WebResourceV1 Bucket", ErrNoBucket)
	}

	return bkt, nil
}

type _IP [16]byte // Enough bytes to contain IPv4 or IPv6.

func convertWebResourceV1sToIPs(resources []storage.WebResourceV1) map[_IP]storage.ResourceNameT {
	var result = make(map[_IP]storage.ResourceNameT)

	for i := range resources {
		pr := &resources[i]

		for _, addr := range pr.Data.IPs {
			result[encodeIP(addr)] = pr.Metadata.Name
		}
	}

	return result
}

func loadAllIPsBkt(bkt *bbolt.Bucket) (map[_IP]storage.ResourceNameT, error) {
	var result = make(map[_IP]storage.ResourceNameT, bkt.Stats().KeyN)

	if err := bkt.ForEach(func(k, v []byte) error {
		result[encodeIP(k)] = storage.ResourceNameT(v)

		return nil
	}); err != nil {
		return nil, err
	}

	return result, nil
}

func setIPsTx(tx *bbolt.Tx, resources []storage.WebResourceV1) error {
	// Bucket.
	bkt, err := getIPsBktTx(tx)
	if err != nil {
		return err
	}

	// Prepare reference data.
	var reference = convertWebResourceV1sToIPs(resources)

	// Load from DB.
	dbIPs, err := loadAllIPsBkt(bkt)
	if err != nil {
		return err
	}

	// Delete.
	for dbK := range dbIPs {
		_, ok := reference[dbK]
		if ok {
			continue
		}

		if err := bkt.Delete(decodeIP(dbK)); err != nil {
			return err
		}
	}

	// Create/Update.
	for refIP, refResName := range reference {
		dbWebRes, ok := dbIPs[refIP]
		if ok {
			// Check for update.
			if dbWebRes == refResName {
				continue
			}
		}

		// Create/Update.
		if err := bkt.Put(decodeIP(refIP), []byte(refResName)); err != nil {
			return err
		}
	}

	return nil
}

func getDomainsBktTx(tx *bbolt.Tx) (*bbolt.Bucket, error) {
	var bkt = tx.Bucket(_DNWResBKT)
	if bkt == nil {
		return nil, fmt.Errorf("%w: Domain => WebResourceV1 Bucket", ErrNoBucket)
	}

	return bkt, nil
}

func convertWebResourceV1sToDomains(resources []storage.WebResourceV1) map[storage.WebResourceDomainT]storage.ResourceNameT {
	var result = make(map[storage.WebResourceDomainT]storage.ResourceNameT, 0)

	for i := range resources {
		pr := &resources[i]

		for j := range pr.Data.Domains {
			result[pr.Data.Domains[j]] = pr.Metadata.Name
		}
	}

	return result
}

func setDomainsTx(tx *bbolt.Tx, resources []storage.WebResourceV1) error {
	// Bucket.
	bkt, err := getDomainsBktTx(tx)
	if err != nil {
		return err
	}

	// Convert.
	var reference = convertWebResourceV1sToDomains(resources)

	var (
		deleteCandidateKeys = make([][]byte, 0)
		updateCandidateKeys = make([]storage.WebResourceDomainT, 0)

		dbKeys = make(map[storage.WebResourceDomainT]bool, bkt.Stats().KeyN)
	)

	if err := bkt.ForEach(func(k, v []byte) error {
		var dbKey = storage.WebResourceDomainT(k)

		refVal, ok := reference[dbKey]
		if ok { // Check for update.
			// Update if not equal.
			if refVal != storage.ResourceNameT(v) {
				updateCandidateKeys = append(updateCandidateKeys, dbKey)
			}

			return nil
		}

		// Delete if not found.
		deleteCandidateKeys = append(deleteCandidateKeys, k)

		return nil
	}); err != nil {
		return err
	}

	// Delete.
	for _, cand := range deleteCandidateKeys {
		if err := bkt.Delete(cand); err != nil {
			return err
		}
	}

	// Update.
	for i := range updateCandidateKeys {
		bkt.Put([]byte(updateCandidateKeys[i]), []byte(reference[updateCandidateKeys[i]]))
	}

	// Create candidates.
	for refK, refVal := range reference {
		_, ok := dbKeys[refK]
		if ok {
			continue
		}

		// Create.
		if err := bkt.Put([]byte(refK), []byte(refVal)); err != nil {
			return err
		}
	}

	return nil
}

func getURLsBktTx(tx *bbolt.Tx) (*bbolt.Bucket, error) {
	var bkt = tx.Bucket(_URLWResBKT)
	if bkt == nil {
		return nil, fmt.Errorf("%w: URL => WebResourceV1 Bucket", ErrNoBucket)
	}

	return bkt, nil
}

func convertWebResourceV1sToURLs(resources []storage.WebResourceV1) map[string]storage.ResourceNameT {
	var result = make(map[string]storage.ResourceNameT, 0)

	for i := range resources {
		pr := &resources[i]

		for j := range pr.Data.URLs {
			result[pr.Data.URLs[j].String()] = pr.Metadata.Name
		}
	}

	return result
}

func setURLsTx(tx *bbolt.Tx, resources []storage.WebResourceV1) error {
	// Get Bucket.
	bkt, err := getURLsBktTx(tx)
	if err != nil {
		return err
	}

	// Convert Reference.
	var reference = convertWebResourceV1sToURLs(resources)

	var (
		deleteCandidateKeys = make([][]byte, 0)
		updateCandidateKeys = make([]string, 0)

		dbKeys = make(map[string]bool, bkt.Stats().KeyN)
	)

	// Update and delete candidates.
	if err := bkt.ForEach(func(k, v []byte) error {
		var dbURL = string(k)

		// Add to DB Keys.
		dbKeys[dbURL] = true

		refV, ok := reference[dbURL]
		if ok {
			// Compare for update.
			if refV != storage.ResourceNameT(v) {
				updateCandidateKeys = append(updateCandidateKeys, dbURL)
			}

			return nil
		}

		// Delete.
		deleteCandidateKeys = append(deleteCandidateKeys, k)

		return nil
	}); err != nil {
		return err
	}

	// Delete all required.
	for i := range deleteCandidateKeys {
		if err := bkt.Delete(deleteCandidateKeys[i]); err != nil {
			return err
		}
	}

	// Update all required.
	for i := range updateCandidateKeys {
		if err := bkt.Put([]byte(updateCandidateKeys[i]), []byte(reference[updateCandidateKeys[i]])); err != nil {
			return err
		}
	}

	// Create.
	for refK, refV := range reference {
		if dbKeys[refK] { // Skip existing.
			continue
		}

		if err := bkt.Put([]byte(refK), []byte(refV)); err != nil {
			return err
		}
	}

	return nil
}

func getWebResourceToWebResourceCategoriesBktTx(tx *bbolt.Tx) (*bbolt.Bucket, error) {
	var bkt = tx.Bucket(_WResWResCatBKT)
	if bkt == nil {
		return nil, fmt.Errorf("%w: WebResourceV1 => WebResourceCategoryV1 Bucket", ErrNoBucket)
	}

	return bkt, nil
}

func convertWebResourceV1sToWebResourceCategories(
	resources []storage.WebResourceV1) map[storage.ResourceNameT]storage.ResourceNameT {
	var result = make(map[storage.ResourceNameT]storage.ResourceNameT, len(resources))

	for i := range resources {
		pr := &resources[i]

		result[pr.Metadata.Name] = pr.Data.WebResourceCategoryName
	}

	return result
}

func setWebResourceCategoriesV1Tx(tx *bbolt.Tx, resources []storage.WebResourceV1) error {
	// Bucket.
	bkt, err := getWebResourceToWebResourceCategoriesBktTx(tx)
	if err != nil {
		return err
	}

	// Convert reference data.
	var reference = convertWebResourceV1sToWebResourceCategories(resources)

	var (
		deleteCandidateKeys = make([][]byte, 0)
		updateCandidateKeys = make([]storage.ResourceNameT, 0)

		dbKeys = make(map[storage.ResourceNameT]bool, bkt.Stats().KeyN)
	)

	// Update and delete candidates.
	if err := bkt.ForEach(func(k, v []byte) error {
		var dbWebResource = storage.ResourceNameT(k)

		// Add to DB Keys.
		dbKeys[dbWebResource] = true

		refV, ok := reference[dbWebResource]
		if ok {
			// Compare for update.
			if refV != storage.ResourceNameT(v) {
				updateCandidateKeys = append(updateCandidateKeys, dbWebResource)
			}

			return nil
		}

		// Delete.
		deleteCandidateKeys = append(deleteCandidateKeys, k)

		return nil
	}); err != nil {
		return err
	}

	// Delete all required.
	for i := range deleteCandidateKeys {
		if err := bkt.Delete(deleteCandidateKeys[i]); err != nil {
			return err
		}
	}

	// Update all required.
	for i := range updateCandidateKeys {
		if err := bkt.Put([]byte(updateCandidateKeys[i]), []byte(reference[updateCandidateKeys[i]])); err != nil {
			return err
		}
	}

	// Create.
	for refK, refV := range reference {
		if dbKeys[refK] { // Skip existing.
			continue
		}

		if err := bkt.Put([]byte(refK), []byte(refV)); err != nil {
			return err
		}
	}

	return nil
}

func setWebResourcesV1Tx(tx *bbolt.Tx, resources []storage.WebResourceV1) error {
	// Set IPs.
	if err := setIPsTx(tx, resources); err != nil {
		return err
	}

	// Set Domains.
	if err := setDomainsTx(tx, resources); err != nil {
		return err
	}

	// Set URLs.
	if err := setURLsTx(tx, resources); err != nil {
		return err
	}

	// Set Web Resource Categories.
	if err := setWebResourceCategoriesV1Tx(tx, resources); err != nil {
		return err
	}

	return nil
}

func getURLWebResourceTx(tx *bbolt.Tx, u *url.URL) ([]byte, error) {
	// Bucket.
	bkt, err := getURLsBktTx(tx)
	if err != nil {
		return nil, err
	}

	// Resource.
	var raw = bkt.Get([]byte(u.String()))
	if raw == nil {
		return nil, fmt.Errorf("%w: URL %s", ErrNotFound, u.String())
	}

	return raw, nil
}

func getDomainWebResourceTx(tx *bbolt.Tx, domain storage.WebResourceDomainT) ([]byte, error) {
	// Bucket.
	bkt, err := getDomainsBktTx(tx)
	if err != nil {
		return nil, err
	}

	// Resource.
	var raw = bkt.Get([]byte(domain))
	if raw == nil {
		return nil, fmt.Errorf("%w: Domain %s", ErrNotFound, domain)
	}

	return raw, nil
}

func getIPWebResourceTx(tx *bbolt.Tx, IP net.IP) ([]byte, error) {
	// Bucket.
	bkt, err := getIPsBktTx(tx)
	if err != nil {
		return nil, err
	}

	// Resource.
	var addr = encodeIP(IP)

	var raw = bkt.Get(addr[:])
	if raw == nil {
		return nil, fmt.Errorf("%w: IP %s", ErrNotFound, IP.String())
	}

	return raw, nil
}

func getWebResourceWebResourceCategoryTx(tx *bbolt.Tx, webResource []byte) ([]byte, error) {
	// Bucket.
	bkt, err := getWebResourceToWebResourceCategoriesBktTx(tx)
	if err != nil {
		return nil, err
	}

	// Load.
	var cat = bkt.Get(webResource)
	if cat == nil {
		return nil, fmt.Errorf("%w: WebResourceCategoryV1 for WeResourceV1 %s",
			ErrNotFound, string(webResource))
	}

	return cat, nil
}

type webResourceInfo struct {
	accessGroups [][]byte
	resource     storage.ResourceNameT
	category     storage.ResourceNameT
}

// getURLAccessGroupV1sNamesTx loads and returns AccessGroupV1 Names for given URL.
// Lookup is based on a chain: URL => WebResourceV1 => WebResourceCategoryV1 => AccessGroupV1.
func getURLAccessGroupV1sNamesTx(tx *bbolt.Tx, u *url.URL) (*webResourceInfo, error) {
	// Get WebResourceV1.
	resource, err := getURLWebResourceTx(tx, u)
	if err != nil {
		return nil, err
	}

	// Get WebResourceCategoryV1.
	category, err := getWebResourceWebResourceCategoryTx(tx, resource)
	if err != nil {
		return nil, err
	}

	// Get AccessGroupV1 Names.
	names, err := getWebResourceCategoryV1AccessGroupV1sNamesTx(tx, category)
	if err != nil {
		return nil, err
	}

	return &webResourceInfo{
		accessGroups: names,
		category:     storage.ResourceNameT(category),
		resource:     storage.ResourceNameT(resource),
	}, nil
}

// getDomainAccessGroupV1sNamesTx loads and returns AccessGroupV1 Names for given URL.
// Lookup is based on a chain: Domain Name => WebResourceV1 => WebResourceCategoryV1 => AccessGroupV1.
func getDomainAccessGroupV1sNamesTx(tx *bbolt.Tx, domain storage.WebResourceDomainT) (*webResourceInfo, error) {
	// Get WebResourceV1.
	resource, err := getDomainWebResourceTx(tx, domain)
	if err != nil {
		return nil, err
	}

	// Get WebResourceCategoryV1.
	category, err := getWebResourceWebResourceCategoryTx(tx, resource)
	if err != nil {
		return nil, err
	}

	// Get AccessGroupV1 Names.
	names, err := getWebResourceCategoryV1AccessGroupV1sNamesTx(tx, category)
	if err != nil {
		return nil, err
	}

	return &webResourceInfo{
		accessGroups: names,
		category:     storage.ResourceNameT(category),
		resource:     storage.ResourceNameT(resource),
	}, nil
}

// getIPAccessGroupV1sNamesTx loads and returns AccessGroupV1 Names for given URL.
// Lookup is based on a chain: IP => WebResourceV1 => WebResourceCategoryV1 => AccessGroupV1.
func getIPAccessGroupV1sNamesTx(tx *bbolt.Tx, IP net.IP) (*webResourceInfo, error) {
	// Get WebResourceV1.
	resource, err := getIPWebResourceTx(tx, IP)
	if err != nil {
		return nil, err
	}

	// Get WebResourceCategoryV1.
	category, err := getWebResourceWebResourceCategoryTx(tx, resource)
	if err != nil {
		return nil, err
	}

	// Get AccessGroupV1 Names.
	names, err := getWebResourceCategoryV1AccessGroupV1sNamesTx(tx, category)
	if err != nil {
		return nil, err
	}

	return &webResourceInfo{
		accessGroups: names,
		category:     storage.ResourceNameT(category),
		resource:     storage.ResourceNameT(resource),
	}, nil
}

package db

import (
	"net"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/confreader/types"
	"go.etcd.io/bbolt"
)

func checkAccessUserIPTx(tx *bbolt.Tx, source, username storage.ResourceNameT, IP net.IP) (*types.CheckResult, error) {
	// Find a first AccessGroupV1 for given User.
	userAccessGroup, err := getUserFirstAccessGroupV1Tx(tx, source, username)
	if err != nil {
		return nil, err
	}

	// Lookup for IP's Info.
	IPInfo, err := getIPAccessGroupV1sNamesTx(tx, IP)
	if err != nil {
		return nil, err
	}

	return &types.CheckResult{
		Allowed:             computeAccessPolicyResult(userAccessGroup, IPInfo.accessGroups),
		AccessGroup:         userAccessGroup,
		Source:              source,
		User:                username,
		WebResource:         IPInfo.resource,
		WebResourceCategory: IPInfo.category,
	}, nil
}

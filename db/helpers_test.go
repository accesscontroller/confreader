package db

import (
	"crypto/rand"
	"math/big"
	"net"
	"net/url"
	"os"
	"reflect"

	"github.com/bxcodec/faker/v3"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.etcd.io/bbolt"
)

const testDBPath = "test-db.bbolt"

func initDBTest() (*DB, error) {
	// Remove existing DB.
	if err := os.Remove(testDBPath); err != nil {
		if !os.IsNotExist(err) {
			return nil, err
		}
	}

	// Open DB.
	db, err := New(testDBPath)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func initDBTestNoBuckets() (*DB, error) {
	// Remove existing DB.
	if err := os.Remove(testDBPath); err != nil {
		if !os.IsNotExist(err) {
			return nil, err
		}
	}

	// Open DB.
	db, err := bbolt.Open(testDBPath, 777, bbolt.DefaultOptions)
	if err != nil {
		return nil, err
	}

	return &DB{
		db: db,
	}, nil
}

var (
	testIsFakerConfigured bool // nolint:gochecknoglobals
)

func fakerCustomGenerator() error { // nolint:gocognit,gocyclo
	if testIsFakerConfigured {
		return nil
	}

	faker.SetGenerateUniqueValues(true)

	const mapSliceMaxLength = 10

	if err := faker.SetRandomMapAndSliceSize(mapSliceMaxLength); err != nil {
		return err
	}

	if err := faker.SetRandomNumberBoundaries(1, 10000000); err != nil {
		return err
	}

	if err := faker.AddProvider("api_version", func(v reflect.Value) (interface{}, error) {
		return "v1", nil
	}); err != nil {
		return err
	}

	if err := faker.AddProvider("e_tag", func(v reflect.Value) (interface{}, error) {
		val, err := rand.Int(rand.Reader, big.NewInt(2048))
		if err != nil {
			return nil, err
		}

		return storage.ETagT(val.Uint64()), nil
	}); err != nil {
		return err
	}

	if err := faker.AddProvider("default_access_policy", func(v reflect.Value) (interface{}, error) {
		val, err := rand.Int(rand.Reader, big.NewInt(2))
		if err != nil {
			return nil, err
		}

		var res = storage.DefaultAccessPolicyT(val.Uint64())

		res++ // DefaultAccessPolicyT has value either 1 or 2.

		return res, nil
	}); err != nil {
		return err
	}

	if err := faker.AddProvider("web_resource_domain_names", func(v reflect.Value) (interface{}, error) {
		val, err := rand.Int(rand.Reader, big.NewInt(mapSliceMaxLength))
		if err != nil {
			return nil, err
		}

		var iVal = val.Int64()

		var domains = make([]storage.WebResourceDomainT, iVal)

		for i := range domains {
			domains[i] = storage.WebResourceDomainT(faker.DomainName())
		}

		return domains, nil
	}); err != nil {
		return err
	}

	if err := faker.AddProvider("web_resource_urls", func(v reflect.Value) (interface{}, error) {
		val, err := rand.Int(rand.Reader, big.NewInt(mapSliceMaxLength))
		if err != nil {
			return nil, err
		}

		var iVal = val.Int64()

		var urls = make([]url.URL, iVal)

		for i := range urls {
			us := faker.URL()

			u, err := url.Parse(us)
			if err != nil {
				return nil, err
			}

			urls[i] = *u
		}

		return urls, nil
	}); err != nil {
		return err
	}

	if err := faker.AddProvider("web_resource_ips", func(v reflect.Value) (interface{}, error) {
		val, err := rand.Int(rand.Reader, big.NewInt(mapSliceMaxLength))
		if err != nil {
			return nil, err
		}

		var iVal = val.Int64()

		var ips = make([]net.IP, iVal)

		for i := range ips {
			ipVer, err := rand.Int(rand.Reader, big.NewInt(2)) // 0 - IPv6, 1 - IPv4.
			if err != nil {
				return nil, err
			}

			var ipS string

			if ipVer.Int64() == 1 {
				ipS = faker.IPv4()
			} else {
				ipS = faker.IPv6()
			}

			ips[i] = net.ParseIP(ipS)
		}

		return ips, nil
	}); err != nil {
		return err
	}

	if err := faker.AddProvider("web_resource_web_resource_category", func(v reflect.Value) (interface{}, error) {
		// One Of.
		var possibleCategories = map[int64]string{
			0: "for web resources create web resource category 117",
			1: "for web resources create web resource category 118",
			2: "for web resources create web resource category 119",
		}

		wResCatID, err := rand.Int(rand.Reader, big.NewInt(3))
		if err != nil {
			return nil, err
		}

		return possibleCategories[wResCatID.Int64()], nil
	}); err != nil {
		return err
	}

	testIsFakerConfigured = true

	return nil
}

func parseURLPanic(u string) url.URL {
	p, err := url.Parse(u)
	if err != nil {
		panic(err)
	}

	return *p
}

func parseIPPanic(addr string) net.IP {
	_IP := net.ParseIP(addr)
	if _IP == nil {
		panic("Can not parse IP")
	}

	return _IP
}

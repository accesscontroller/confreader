package confreader

import (
	"fmt"

	"go.uber.org/zap"
	"gopkg.in/robfig/cron.v2"
)

func reloadAccessGroupsV1(cl APIAccessGroupV1sGetter, st LocalAccessGroupV1sSetter) error {
	srvData, err := cl.GetAccessGroups(nil)
	if err != nil {
		return fmt.Errorf("can not load API AccessGroupListV1: %w", err)
	}

	if err = st.SetAccessGroupV1s(srvData); err != nil {
		return fmt.Errorf("can not update internal AccessGroupV1 storage: %w", err)
	}

	return nil
}

func wrapReloadAccessGroupsV1(cl APIAccessGroupV1sGetter, st LocalAccessGroupV1sSetter, logger *zap.Logger) cron.Job {
	iLog := logger.With(zap.String("job", "ReloadAccessGroups"))

	return cron.FuncJob(func() {
		if err := reloadAccessGroupsV1(cl, st); err != nil {
			iLog.Error("Can not reload AccessGroupsV1", zap.Error(err))
		} else {
			iLog.Info("Reloaded AccessGroupsV1")
		}
	})
}

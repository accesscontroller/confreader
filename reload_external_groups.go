package confreader

import (
	"fmt"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.uber.org/zap"
	"gopkg.in/robfig/cron.v2"
)

func reloadExternalGroups(sources []storage.ResourceNameT,
	cl APIExternalGroupV1sExternalUsersV1sGetter,
	st LocalExternalGroupV1sExternalUsersV1sSetter) error {
	// Load Groups.
	var (
		resSrvGroups = make([]storage.ExternalGroupV1, 0)
		resRelations = make([]*storage.ExternalGroup2ExternalUserListRelationsV1, 0)
	)

	for _, src := range sources {
		iSrvGroups, err := cl.GetExternalGroups(nil, src)
		if err != nil {
			return fmt.Errorf("can not load API ExternalGroupListV1: %w", err)
		}

		resSrvGroups = append(resSrvGroups, iSrvGroups...)

		// For every group load relations.
		var iRelations = make([]*storage.ExternalGroup2ExternalUserListRelationsV1, 0, len(iSrvGroups))

		for i := range iSrvGroups {
			pg := &iSrvGroups[i]

			rels, err := cl.GetExternalGroup2ExternalUserListRelationsV1(
				pg.Metadata.Name, pg.Metadata.ExternalSource.SourceName)
			if err != nil {
				return fmt.Errorf("can not load ExternalGroup2ExternalUserListV1Relations for Group %s:%s: %w",
					pg.Metadata.ExternalSource.SourceName, pg.Metadata.Name, err)
			}

			iRelations = append(iRelations, rels)
		}

		resRelations = append(resRelations, iRelations...)
	}

	// Set Groups.
	if err := st.SetExternalGroupV1s(resSrvGroups); err != nil {
		return fmt.Errorf("can not update internal ExternalGroupV1 storage: %w", err)
	}

	// Set relations.
	if err := st.SetExternalGroup2ExternalUserListRelationsV1(resRelations); err != nil {
		return fmt.Errorf(
			"can not update internal ExternalGroup2ExternalUserListV1Relations storage: %w", err)
	}

	return nil
}

func wrapReloadExternalGroupsV1(
	sources []storage.ResourceNameT,
	cl APIExternalGroupV1sExternalUsersV1sGetter,
	st LocalExternalGroupV1sExternalUsersV1sSetter, logger *zap.Logger) cron.Job {
	iLog := logger.With(zap.String("job", "ReloadExternalGroups"))

	return cron.FuncJob(func() {
		if err := reloadExternalGroups(sources, cl, st); err != nil {
			iLog.Error("Can not reload ExternalGroupsV1", zap.Error(err))
		} else {
			iLog.Info("Reloaded ExternalGroupsV1")
		}
	})
}
